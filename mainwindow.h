/**********************************************************************
        Copyright 2014 Shannon Mackey

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

        http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
**********************************************************************/

#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QFile>
#include <QMainWindow>
#include <QStandardItem>
#include <QStandardItemModel>
#include <QSortFilterProxyModel>
#include <QStringListModel>
#include <QXmlStreamReader>
#include <QSqlQueryModel>
#include <QSqlQuery>

#include "pentable.h"
#include "idftranslator.h"

#include "xmlsyntaxhighlighter.h"

namespace Ui {
class MainWindow;
}

const QString ROOT_SCHEMA_TAG = "iddx";
const QString ROOT_DATA_TAG = "idfx";

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();


    void nameChanges();


    void exportJSON(bool nice = false);
private slots:
    void on_action_Quit_triggered();

    //schema
    void putIDDintoPenTable(QString idd_filename);
    QStringList extractCleanStringListFromFile(QFile &idd);
    QString cropLineType(QString line, QString type);
    void loadIDDSchema(QFile &idd);
    void loadSchemaObjectModelFromDB(QString schema_name);
    void resetIddModelGeneration(QString filename);
    void submitSchemaViewQuery(QString qry);
    void submitDataViewQuery(QString qry);
    void executeQueryStringList(QStringList query_strings);
    void alterschema_PropertyName(QSqlQuery props, QStringList &sql_strings, QString badstring, QString goodstring);
    void updateSchema();

    //model
    void putIDFintoTranslator(QString idf_filename);
    void loadDataObjectModelFromDB(QString model_name);

    // xsd
    void generateIDDxSchema();
    void insertSchemaRootElementList(QXmlStreamWriter &schema, QStringList object_names);
    void insertSchemaHeader(QXmlStreamWriter &schema);
    QStringList getSchemaObjectNamesList(QString schema_name);
    void insertSchemaUuidAttributeType(QXmlStreamWriter &schema);
    void insertSchemaOrderAttributeType(QXmlStreamWriter &schema);
    void insertSchemaUniqueAttributeTypes(QXmlStreamWriter &schema);
    void insertSchemaVendorObject(QXmlStreamWriter &schema);

    void insertSchemaFooter(QXmlStreamWriter &schema);
    void insertSchemaAllObjectElements(QXmlStreamWriter &schema);
    void generateXSDv2();

    //xml
    void generateXMLv2();
    void writeQObjectXML(QXmlStreamWriter &xml, QObject *head);


    //sql tab
    void on_sql_selector_currentIndexChanged(const QString &newview);
    void on_sql_filter_returnPressed();
    void on_SQL_view_doubleClicked(const QModelIndex &index);
    void on_sql_idd_edit_textEdited(const QString &idd);

    void on_sql_selector_2_currentIndexChanged(const QString &newview);
    void on_sql_filter_2_returnPressed();
    void on_SQL_view_2_doubleClicked(const QModelIndex &index);
    void on_sql_idf_edit_textEdited(const QString &idf);

    void on_sql_generate_clicked();
    void on_actionDump_Fat_DB_triggered();

    //xml tab
    void on_xml_xsd_textChanged();
    void on_xml_xml_textChanged();
    void on_xml_xsd_file_textEdited(const QString &xsd);
    void on_xml_xml_file_textEdited(const QString &xml);
    void on_xml_xsd_file_textChanged(const QString &xsd);
    void on_xml_xml_file_textChanged(const QString &arg1);
    void loadXsdView(const QString &xsd);
    void loadXmlView(const QString &xml);
    void on_xml_v2_clicked();

    bool validateXml();
    void on_saveXSD_clicked();

    //sql log
    QString getWhatLogEntrySql(QString uuid,
                               QString event,
                               QString subject_type,
                               QString subject_name,
                               QString subject_uuid,
                               QString old_value = "",
                               QString new_value = "" );
    QString getWhyLogEntrySql(QString uuid,
                              QString why,
                              QString note = "");

    //sql schema alteration
    void datatypeChanges();
    void alterSchema_EveryObjectHasNewUuidField();
    void alterSchema_RemoveObsolete();
    void alterSchema_RemoveDeprecated();
    void alterSchema_NameToLabel();
    void alterSchema_IllegalCharacterFix();
    void alterSchema_ObjectListEnumerations();
    void alterSchema_DeprecateRetaincase();
    void alterSchema_DeprecateRequiredField();
    void alterSchema_SetLabelDataType();
    void alterSchema_DataTypeCase();
    void alterSchema_AlphaDataTypeSpecifierA();
    void alterSchema_SetDataTypeRealByValues();
    void alterSchema_SetDataTypeIntegerByValues();
    void alterSchema_SetDataTypeNameInference();
    void alterSchema_SetMissingUnitDataTypes();
    void alterSchema_SetDataTypeObjectList();
    void alterSchema_SetDataTypeExternalList();
    void alterSchema_SetOptionalRequired();
    void alterSchema_EnsureAttributeDataTypeEntries();


    void alterSchema_MarkEnumerationsExtensible();
    void alterSchema_ExtensibleExtraction();
    void alterSchema_InsertInclusionTypes();


    void on_sql_idfx_generate_clicked();

    void on_schema_validate_clicked();

    void on_xml_pretty_clicked();

    void on_xml_xsd_find_textChanged(const QString &arg1);

    void on_xml_xml_find_textChanged(const QString &arg1);

    void on_xml_xml_find_textEdited(const QString &arg1);

    void on_xml_highlight_clicked();

    void on_xml_xsd_find_textEdited(const QString &arg1);

    void on_xsd_highlight_clicked();

    void on_pushButton_clicked();

    void on_pushButton_2_clicked();

    void on_pushButton_3_clicked();

    void on_pushButton_4_clicked();

    void on_pushButton_5_clicked();

private:
    Ui::MainWindow *ui;

    PenTable *Data;
    IDFtranslator *IDF;

    QObject *root_schema_object;
    QObject *root_data_object;

    QSortFilterProxyModel *Schema_proxy_model;
    QSortFilterProxyModel *Data_proxy_model;

    XmlSyntaxHighlighter * xml_highlight;
    XmlSyntaxHighlighter * xsd_highlight;
    bool isFirstTimeXML;
    bool isFirstTimeXSD;


signals:
    void reportMessage(QString source_label);
};

#endif // MAINWINDOW_H
