/**********************************************************************
        Copyright 2014 Shannon Mackey

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

        http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
**********************************************************************/

#include "idftranslator.h"

#include <QDir>
#include <QSqlQuery>

#include <QDebug>

IDFtranslator::IDFtranslator(PenTable * pt, QObject *parent) :
    QObject(parent),
    Data(pt)
{
}

void IDFtranslator::loadIDFModel(QFile &idf)
{
    QString oneline;
    while (!idf.atEnd())
    {   //reduce file to list of actual object instances and their fields
        QString nextline = QString::fromLocal8Bit(idf.readLine());
        QStringList bits = nextline.split("!");
        QString split = bits.at(0);//drop all after remark
        oneline.append(split.trimmed());
    }
    QStringList object_instance_lines = oneline.split(";");
    //each object is now a single line
    object_instance_lines.removeAll("");

    ///////// just for debug
    QFile idflines(QDir::homePath() + "/SCRATCH/IDFlines.txt");
    if (idflines.open(QFile::ReadWrite | QFile::Text | QFile::Truncate)) {
        idflines.write(object_instance_lines.join("\n").toLocal8Bit());
    }
    //////////////

    //insert root object as parent to all others
    QString root_uuid = Data->getNewUuidString();
    Data->insertModelObject(root_uuid, "idfx", "");

    //fill extensible types stringlist to check if additional processing is needed per object
    QStringList extensible_list;
    QSqlQuery extensibles = Data->executeQuery(
                QString("SELECT * FROM 'schema_select_extensible_types';"));
    while (extensibles.next()) {
        extensible_list.append(extensibles.value("object_type").toString());
    }

    //process each object line
    foreach (QString one_object, object_instance_lines) {
        QStringList object_fields = one_object.split(",");
        QString object_type = object_fields.takeAt(0);
        QString object_uuid = Data->getNewUuidString();
        //insert new object
        Data->insertModelObject(object_uuid, object_type/*.replace(":","-")*/, root_uuid);
        qDebug() << object_type;
        //get/insert properties
        QSqlQuery field_order = Data->executeQuery(
                    QString("SELECT * FROM 'schema_object_properties_position' "
                            "WHERE object_type = '%1'")
                    .arg(object_type));
        while (field_order.next()) {
            QString field_type = field_order.value("property_type").toString();
            int field_position = field_order.value("position").toInt();
            QString field_uuid = Data->getNewUuidString();
            QString field_val = "UNUSED_OPTIONAL_FIELD";
            if (!object_fields.isEmpty())
            {
                QString input_field = object_fields.takeFirst();
                field_val = (input_field == "") ? "EMPTY_FIELD" : input_field;
            }
            qDebug() << " ------ " << field_val;
            Data->setModelProperty( field_uuid,  field_type,  field_val,  object_uuid);
        }
        //in case it is an extensible object, the object properties stop prior to repeated extensible fields
        //so generate extensibles here, inserted in parent object
        if (extensible_list.contains(object_type)){
            //get extension type for this extensible object
            QSqlQuery ext_object = Data->executeQuery(
                        QString("SELECT * FROM 'schema_select_extensible_extension_types' "
                                "WHERE object_type = '%1'").arg(object_type));
            if (ext_object.first()){
                int order_count = 0;
                QString ext_object_type = ext_object.value("extension_type").toString();


                //get/insert properties
                while (!object_fields.isEmpty()) { //re-querying for while/next

                    //insert new object

                    QString ext_object_uuid = Data->getNewUuidString();
                    Data->insertModelObject(ext_object_uuid, ext_object_type/*.replace(":","-")*/, object_uuid);
                    Data->setModelProperty( Data->getNewUuidString(),  "order",  QString::number(order_count++),  ext_object_uuid);
                    qDebug() << ext_object_type;


                    QSqlQuery ext_field_order = Data->executeQuery(
                                QString("SELECT * FROM 'schema_object_properties_position' "
                                        "WHERE object_type = '%1'")
                                .arg(ext_object_type));
                    //                while (!object_fields.isEmpty()) {
                    qDebug() << object_fields.count();
                    while (ext_field_order.next()) {

                        QString ext_field_type = ext_field_order.value("property_type").toString();
                        int field_position = ext_field_order.value("position").toInt();
                        QString ext_field_uuid = Data->getNewUuidString();
                        QString ext_field_val = "UNUSED_OPTIONAL_FIELD";
                        //                        if (!object_fields.isEmpty())
                        //                        {
                        QString ext_input_field = object_fields.takeFirst();
                        ext_field_val = (ext_input_field == "") ? "EMPTY_FIELD" : ext_input_field;
                        //                        }
                        qDebug()  << " ------ " << ext_field_val;
                        Data->setModelProperty( ext_field_uuid,  ext_field_type,  ext_field_val,  ext_object_uuid);
                    }
                }
            }
        }
    }

    emit loadedfilename(idf.fileName());
}

void IDFtranslator::fixObjectNames()
{
    QSqlQuery enumerated_object_names = Data->executeQuery(
                QString("SELECT property_uuid, property_type, property_value, object_uuid "
                        "FROM 'model_object_properties_attributes' "
                        "where attribute_type = 'choices' and property_value like '%:%'"));
    while (enumerated_object_names.next()) {
        QString property_uuid = enumerated_object_names.value("property_uuid").toString();
        QString property_type = enumerated_object_names.value("property_type").toString();
        QString property_value = enumerated_object_names.value("property_value").toString();
        QString model_object_uuid = enumerated_object_names.value("object_uuid").toString();
//        property_value.replace(":","-");
        qDebug() << property_value;
        Data->setModelProperty(property_uuid, property_type, property_value, model_object_uuid);
    }
}

void IDFtranslator::translate()
{
    //data corrections

  //  fixObjectNames();







    //make constructions extensible correctly



    //identify how many other objects have references to a particular object-list
    //if more than one, then keep separate list container
    //if only one, then put those objects into the referring container's bucket,
    //and set the referring field to the referring container's uuid
QString all_object_list_refs("SELECT distinct object_type, property_type, property_value "
                             "FROM 'model_object_properties_attributes' "
                             "where attribute_type = 'data_type' and attribute_value like '%list'");




    //object-lists, recreate in generic list containers
    //set Type and Label properties on container
    //load list containers with objects


    //nodes? node-list


    //    remove OPTIONAL fields
    //            remove BLANK fields


    Data->saveToDisk(QDir::homePath() + "/SCRATCH/IDFx.sqlt");
}
