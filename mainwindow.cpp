/**********************************************************************
        Copyright 2014 Shannon Mackey

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

        http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
**********************************************************************/

#include "mainwindow.h"
#include "ui_mainwindow.h"

#include <QSqlDatabase>
#include <QSqlQuery>
#include <QSqlRecord>
#include <QSqlError>
#include <QDebug>
#include <QFile>
#include <QXmlStreamWriter>
#include <QUuid>
#include <QBuffer>
#include <QXmlFormatter>
#include <QDir>
#include <QAbstractMessageHandler>
#include <QXmlSchemaValidator>
#include <QXmlSchema>
#include <QTime>
#include <QTimer>
#include <QFileDialog>
#include <QElapsedTimer>
#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonArray>
#include <QJsonValue>


// deliberately not refactoring this utility into further classes,
// when this schema generator utility has done its job
// components will find their way into the translator utility
// correctly factored into appropriate classes
// also, there are very few checks

const QString EP = "http://energyplus.doe.gov/2014/XMLSchema";
const QString XS = "http://www.w3.org/2001/XMLSchema";

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent), ui(new Ui::MainWindow),
      Data(new PenTable()),
      IDF(new IDFtranslator(Data))
{
    ui->setupUi(this);

  //  resetIddModelGeneration("://resources/Energy+v8-2.iddx.txt");
    //    resetIddModelGeneration("://resources/IDD_training_set.idd");

    resetIddModelGeneration(QFileDialog::getOpenFileName(this, "IDD","/home/minsky","IDD (*.idd)"));

    ui->SQL_view->resizeColumnsToContents();
    // xml niceties
    on_xml_highlight_clicked();
    on_xsd_highlight_clicked();

    connect(IDF,SIGNAL(loadedfilename(QString)),
            ui->sql_idf_edit, SLOT(setText(QString)));
    connect(this, SIGNAL(reportMessage(QString)),
            ui->statusBar, SLOT(showMessage(QString)));


    isFirstTimeXML = true;
    isFirstTimeXSD = true;
}

MainWindow::~MainWindow() {
    delete ui;
    delete root_schema_object;
    delete root_data_object;
}

void MainWindow::on_action_Quit_triggered() {
    close();
}

void MainWindow::resetIddModelGeneration(QString filename) {

    putIDDintoPenTable(filename);
    Data->saveToDisk(QString(QDir::homePath() + "/SCRATCH/pentable" +
                             QTime::currentTime().toString().remove(":") +
                             ".sqlt"));

    foreach(QString view, Data->getViews("schema")) {
        ui->sql_selector->insertItem(ui->sql_selector->count(), view);
    }
    foreach(QString view, Data->getViews("model")) {
        ui->sql_selector_2->insertItem(ui->sql_selector_2->count(), view);
    }
    Schema_proxy_model = new QSortFilterProxyModel();
    Schema_proxy_model->setSourceModel(Data->getSchemaViewModel());
    ui->SQL_view->setModel(Schema_proxy_model);

    Data_proxy_model = new QSortFilterProxyModel();
    Data_proxy_model->setSourceModel(Data->getDataViewModel());
    ui->SQL_view_2->setModel(Data_proxy_model);
}

void MainWindow::loadSchemaObjectModelFromDB(QString schema_name) {
    qDebug("generateSchemaObjectModel");

    root_schema_object = Data->getSchemaObjectModel(schema_name);

    emit reportMessage("loadSchemaObjectModelFromDB");
}



void MainWindow::loadDataObjectModelFromDB(QString model_name)
{
    qDebug("loadDataObjectModelFromDB");

    root_data_object = Data->getDataObjectModel(model_name);

    emit reportMessage("loadDataObjectModelFromDB");

}




bool MainWindow::validateXml() {
    const QByteArray schemaData = ui->xml_xsd->toPlainText().toUtf8();
    const QByteArray instanceData = ui->xml_xml->toPlainText().toUtf8();

    MessageHandler messageHandler;

    QXmlSchema schema;
    schema.setMessageHandler(&messageHandler);
    schema.load(schemaData);

    bool errorOccurred = false;
    if (!schema.isValid()) {
        errorOccurred = true;
        ui->xml_status->setTextBackgroundColor(Qt::red);
    } else {
        QXmlSchemaValidator validator(schema);
        if (!validator.validate(instanceData))
            errorOccurred = true;
        ui->xml_status->setTextBackgroundColor(Qt::white);
    }

    if (errorOccurred) {
        ui->xml_status->setText(messageHandler.statusMessage());
    } else {
        ui->xml_status->setText(tr("validation successful"));
    }

    const QString styleSheet =
            QString("QTextBrowser {background: %1; padding: 3px}")
            .arg(errorOccurred ? QColor(Qt::red).lighter(160).name()
                               : QColor(Qt::green).lighter(160).name());
    ui->xml_status->setStyleSheet(styleSheet);

    emit reportMessage("validate!");
}

void MainWindow::on_xml_xsd_textChanged() {
    validateXml();
}

void MainWindow::on_xml_xml_textChanged() {
    validateXml();
}

void MainWindow::on_xml_v2_clicked() {
    //    generateXSDv2();

    generateXSDv2();
    generateXMLv2();
}

void MainWindow::insertSchemaRootElementList(QXmlStreamWriter &schema,
                                             QStringList object_names) {
    //////////////////////////////////////////////////  object refs
    //                /////////////////
    //            <xs:element name="idfx">
    //                <xs:complexType>
    //                  <xs:choice  minOccurs="0" maxOccurs="unbounded">
    //                    <xs:element ref="ep:Building"/>
    //                    <xs:element ref="ep:BuildingSurface_Detailed"/>
    //                    <xs:element ref="ep:Vertex"/>
    //                  </xs:choice>
    //                </xs:complexType>
    //             </xs:element>
    //                /////////////////
    schema.writeStartElement("xs:element");
    schema.writeAttribute("name", "idfx");
    schema.writeStartElement("xs:complexType");
    schema.writeStartElement("xs:choice");
    schema.writeAttribute("minOccurs", "0");
    schema.writeAttribute("maxOccurs", "unbounded");
    foreach(QString one_object_name, object_names) {
        schema.writeStartElement("xs:element");
        schema.writeAttribute("ref", "ep:" + one_object_name);
        schema.writeEndElement(); // element
    }
    schema.writeStartElement("xs:element");
    schema.writeAttribute("ref", "ep:VendorObject");
    schema.writeEndElement(); // VendorObject element
    schema.writeEndElement(); // choice
    schema.writeEndElement(); // complexType
    schema.writeEndElement();
}

void MainWindow::insertSchemaHeader(QXmlStreamWriter &schema) {
    schema.writeStartDocument("1.0");
    schema.writeStartElement("xs:schema");
    schema.writeAttribute("elementFormDefault", "unqualified");
    schema.writeNamespace(XS, "xs");
    schema.writeNamespace(EP, "ep");
    //    schema.writeDefaultNamespace(EP);
    schema.writeAttribute("targetNamespace", EP);
    schema.setAutoFormatting(true);
}

QStringList MainWindow::getSchemaObjectNamesList(QString schema_name) {
    QStringList object_names;
    QSqlQuery get_distinct_objects =
            Data->executeQuery(QString("SELECT object_type FROM schema_objects"));
    while (get_distinct_objects.next()) {
        object_names.append(get_distinct_objects.value("object_type").toString());
    }
    return object_names;
}

void MainWindow::insertSchemaUuidAttributeType(QXmlStreamWriter &schema) {
    //            <xs:attribute name="uuid">
    //                <xs:simpleType>
    //                    <xs:restriction base="xs:string">
    //                        <xs:pattern
    // value="[a-f0-9]{8}-[a-f0-9]{4}-[a-f0-9]{4}-[a-f0-9]{4}-[a-f0-9]{12}"/>
    //                    </xs:restriction>
    //                </xs:simpleType>
    //            </xs:attribute>
    schema.writeStartElement("xs:attribute");
    schema.writeAttribute("name", "uuid");
    schema.writeStartElement("xs:simpleType");
    schema.writeStartElement("xs:restriction");
    schema.writeAttribute("base", "xs:string");
    schema.writeStartElement("xs:pattern");
    schema.writeAttribute(
                "value",
                "[a-f0-9]{8}-[a-f0-9]{4}-[a-f0-9]{4}-[a-f0-9]{4}-[a-f0-9]{12}");
    schema.writeEndElement(); // pattern
    schema.writeEndElement(); // restriction
    schema.writeEndElement(); // simpleType
    schema.writeEndElement();
}

void MainWindow::insertSchemaOrderAttributeType(QXmlStreamWriter &schema) {
    //            <xs:attribute name="order">
    //                    <xs:simpleType>
    //                      <xs:restriction base="xs:integer">
    //                        <xs:minInclusive value="0"/>
    //                        <xs:maxInclusive value="100"/>
    //                      </xs:restriction>
    //                    </xs:simpleType>
    //            </xs:attribute>
    schema.writeStartElement("xs:attribute");
    schema.writeAttribute("name", "order");
    schema.writeStartElement("xs:simpleType");
    schema.writeStartElement("xs:restriction");
    schema.writeAttribute("base", "xs:integer");
    schema.writeStartElement("xs:minInclusive");
    schema.writeAttribute("value","0");
    schema.writeEndElement(); // minInclusive
    schema.writeEndElement(); // restriction
    schema.writeEndElement(); // simpleType
    schema.writeEndElement();
}

void MainWindow::insertSchemaFooter(QXmlStreamWriter &schema) {
    schema.writeEndElement(); // complexType
    schema.writeEndElement();
}

void MainWindow::insertSchemaAllObjectElements(QXmlStreamWriter &schema) {
    ////////////////////////////////////////////////  objects, themselves
    /// this, seemingly, might go inside the object itself.
    /// actually, any possible elegance of self xsd describing objects is
    /// overridden by the need, often enough, to know the values of external
    /// objects/fields to make decisions on how to write one particular
    /// object/field

    //            <xs:element name="Building">
    //              <xs:complexType>
    //                <xs:attribute name="Label" type="xs:string" use="required"/>
    //                <xs:attribute name="Loads_Convergence_Tolerance_Value"
    // use="required">
    //                  <xs:simpleType>
    //                      <xs:restriction base="xs:integer">
    //                          <xs:pattern value="[0-9][0-9][0-9]"/>
    //                      </xs:restriction>
    //                  </xs:simpleType>
    //                </xs:attribute>
    //                ...
    //                <xs:attribute ref="uuid" use="required"/>
    //                <xs:anyAttribute processContents="lax"/>
    //              </xs:complexType>
    //            </xs:element>

    foreach(
            QObject * one_object,
            root_schema_object->children()) {   // this all could be done pure sql, too
        schema.writeStartElement("xs:element");
        QString object_name = one_object->objectName();
        schema.writeAttribute("name", object_name);


        //                <xs:annotation>
        //                  <xs:appinfo>group:	Simulation	Parameters
        // </xs:appinfo>
        //                  <xs:documentation xml:lang="en">Describes	parameters
        // ...</xs:documentation>
        //                </xs:annotation>

        schema.writeStartElement("xs:annotation");
        if (one_object->property("group").toString() != "") {
            QString value("group: ");
            value.append(one_object->property("group").toString());
            schema.writeTextElement(XS, "appinfo", value);
        }
        if (one_object->property("required_object").toString() != "") {
            QString value("required: ");
            value.append(one_object->property("required_object").toString());
            schema.writeTextElement(XS, "appinfo", value);
        }
        if (one_object->property("unique_object").toString() != "") {
            QString value("unique: ");
            value.append(one_object->property("unique_object").toString());
            schema.writeTextElement(XS, "appinfo", value);
        }
        if (one_object->property("obsolete").toString() != "") {
            QString value("obsolete: ");
            value.append(one_object->property("obsolete").toString());
            schema.writeTextElement(XS, "appinfo", value);
        }
        if (one_object->property("memo").toString() != "") {
            schema.writeTextElement(XS, "documentation",
                                    one_object->property("memo").toString());
        }
        schema.writeEndElement(); // annotation
        schema.writeStartElement("xs:complexType");

        //always write choice to include VendorObject, write extension_type only as applicable
        schema.writeStartElement("xs:choice");
        schema.writeAttribute("minOccurs", "0");
        schema.writeAttribute("maxOccurs", "unbounded");
        if (one_object->property("extension_type").toString() != "") {
            QStringList contents = one_object->property("extension_type").toString().split("|");
            foreach(QString one_type, contents) {
                schema.writeStartElement("xs:element");
                schema.writeAttribute("ref", "ep:" + one_type);
                schema.writeEndElement(); // element
            }
        }
        //this requires that VendorObject has been written in a \inclusion to _every_ object
        //        QSqlQuery inclusion_lists = Data->executeQuery(
        //                    QString("SELECT * FROM schema_object_attributes_inclusion "
        //                            "WHERE object_uuid = '%1';")
        //                    .arg(one_object->property("uuid").toString()));
        //        while (inclusion_lists.next()) {
        //            QStringList inclusion_list = inclusion_lists.value("inclusion")
        //                    .toString().split("|");
        //            foreach(QString inclusion_type, inclusion_list){
        schema.writeStartElement("xs:element");
        schema.writeAttribute("ref", "ep:VendorObject");
        schema.writeEndElement(); // element
        //            }
        //        }
        schema.writeEndElement(); // choice


        QObjectList fields = one_object->children();
        foreach(QObject * field, fields) {
            schema.writeStartElement("xs:attribute");
            if (field->objectName() != "order"){
                schema.writeAttribute("name", field->objectName());

                QString req_fld = field->property("required_field").toString();
                qDebug() << req_fld;
                if (req_fld.contains("False")) {
                    schema.writeAttribute("use", "optional");
                }


                bool def = field->dynamicPropertyNames().contains("value_default");
                bool spec = field->dynamicPropertyNames().contains("specifier");
                bool note = field->dynamicPropertyNames().contains("note");
                bool autos = field->dynamicPropertyNames().contains("autosizable");
                bool autoc = field->dynamicPropertyNames().contains("autocalculatable");
                bool ref = field->dynamicPropertyNames().contains("reference");
                bool unit = field->dynamicPropertyNames().contains("units");
                bool dep = field->dynamicPropertyNames().contains("deprecated");
                bool type = field->dynamicPropertyNames().contains("data_type");
                bool pstn = field->dynamicPropertyNames().contains("position");
                bool olist = field->dynamicPropertyNames().contains("object_list");
                bool reqf = field->dynamicPropertyNames().contains("required_field");
                if (def || note || autos || autoc || ref || unit || dep || type || pstn || olist || reqf) {
                    schema.writeStartElement("xs:annotation");
                    if (def) {
                        QString defval("default: ");
                        defval.append(field->property("value_default").toString());
                        schema.writeTextElement(XS, "appinfo", defval);
                    }
                    if (spec) {
                        QString specval("specifier: ");
                        specval.append(field->property("specifier").toString());
                        schema.writeTextElement(XS, "appinfo", specval);
                    }
                    if (autos) {
                        QString asval("autosizable: ");
                        asval.append(field->property("autosizable").toString());
                        schema.writeTextElement(XS, "appinfo", asval);
                    }
                    if (autoc) {
                        QString acval("autocalculatable: ");
                        acval.append(field->property("autocalculatable").toString());
                        schema.writeTextElement(XS, "appinfo", acval);
                    }
                    if (ref) {
                        QString refval("reference: ");
                        refval.append(field->property("reference").toString());
                        schema.writeTextElement(XS, "appinfo", refval);
                    }
                    if (unit) {
                        QString unitval("unit: ");
                        unitval.append(field->property("units").toString());
                        schema.writeTextElement(XS, "appinfo", unitval);
                    }
                    if (dep) {
                        QString depval("deprecated: ");
                        depval.append(field->property("deprecated").toString());
                        schema.writeTextElement(XS, "appinfo", depval);
                    }
                    if (type) {
                        QString typeval("data_type: ");
                        typeval.append(field->property("data_type").toString());
                        schema.writeTextElement(XS, "appinfo", typeval);
                    }
                    if (pstn) {
                        QString posval("index: ");
                        posval.append(field->property("position").toString());
                        schema.writeTextElement(XS, "appinfo", posval);
                    }
                    if (olist) {
                        QString posval("object_list: ");
                        posval.append(field->property("object_list").toString());
                        schema.writeTextElement(XS, "appinfo", posval);
                    }
                    if (reqf) {
                        QString reqfval("required_field: ");
                        reqfval.append(field->property("required_field").toString());
                        schema.writeTextElement(XS, "appinfo", reqfval.toLower());
                    }
                    if (note) {
                        schema.writeTextElement(XS, "documentation",
                                                field->property("note").toString());
                    }
                    schema.writeEndElement(); // annotation
                }
            }
            if (field->objectName() == "order") {
                schema.writeAttribute("ref", "ep:order");
                //        schema.writeAttribute("use", "required");
            } else if (field->property("data_type").toString() == "choice") {
                //                    <xs:simpleType>
                //                            <xs:restriction base="xs:string">
                //                                <xs:enumeration value="Country"/>
                //                                <xs:enumeration value="Suburbs"/>
                //                            </xs:restriction>
                //                    </xs:simpleType>
                schema.writeStartElement("xs:simpleType");
                schema.writeStartElement("xs:restriction");
                schema.writeAttribute("base", "xs:string");
                foreach(QString one_choice,
                        field->property("choices").toString().split("|")) {
                    schema.writeStartElement("xs:enumeration");
                    schema.writeAttribute("value", one_choice);
                    schema.writeEndElement(); // enumeration
                }
                schema.writeEndElement(); // restriction
                schema.writeEndElement(); // simpleType
            } else if (field->property("data_type").toString() ==
                       "external_list") { //FIXME: external-list to external_list- ALTER in db
                //                    <xs:simpleType>
                //                            <xs:restriction base="xs:string">
                //                                <xs:enumeration value="autoRDDvariable
                //                                <xs:enumeration value="autoRDDmeter"/>
                //                                <xs:enumeration
                // value="autoRDDvariableMeter"/>
                //                            </xs:restriction>
                //                    </xs:simpleType>
                schema.writeStartElement("xs:simpleType");
                schema.writeStartElement("xs:restriction");
                schema.writeAttribute("base", "xs:string");
                //                QStringList choices;
                //                choices.append("autoRDDvariable");
                //                choices.append("autoRDDmeter");
                //                choices.append("autoRDDvariableMeter");
                //                foreach(QString one_choice, choices) {
                //                    schema.writeStartElement("xs:enumeration");
                //                    schema.writeAttribute("value", one_choice);
                //                    schema.writeEndElement(); // enumeration
                //                }
                schema.writeEndElement(); // restriction
                schema.writeEndElement(); // simpleType
            } else {
                if ((field->property("data_type").toString() == "integer") ||
                        (field->property("data_type").toString() == "real")) {
                    QString basetype =
                            ((field->property("data_type").toString() == "integer")
                             ? "integer"
                             : "float");
                    schema.writeStartElement("xs:simpleType");
                    schema.writeStartElement("xs:restriction");
                    schema.writeAttribute("base", "xs:" + basetype);
                    QString minex = field->property("exclude_minimum").toString();
                    QString maxex = field->property("exclude_maximum").toString();
                    QString min = field->property("minimum_value").toString();
                    QString max = field->property("maximum_value").toString();
                    if ((minex != "") || (maxex != "") || (min != "") || (max != "")) {
                        if (min != "") {
                            schema.writeStartElement("xs:minInclusive");
                            schema.writeAttribute("value", min);
                            schema.writeEndElement();
                        }
                        if (max != "") {
                            schema.writeStartElement("xs:maxInclusive");
                            schema.writeAttribute("value", max);
                            schema.writeEndElement();
                        }
                        if (minex != "") {
                            schema.writeStartElement("xs:minExclusive");
                            schema.writeAttribute("value", minex);
                            schema.writeEndElement();
                        }
                        if (maxex != "") {
                            schema.writeStartElement("xs:maxExclusive");
                            schema.writeAttribute("value", maxex);
                            schema.writeEndElement();
                        }

                    }
                    schema.writeEndElement(); // restriction
                    schema.writeEndElement(); // simpleType
                }
            }
            //            } //old emptystring test
            schema.writeEndElement(); // attribute
        }




        // uuid attribute on the object itself
        schema.writeStartElement("xs:attribute");
        schema.writeAttribute("ref", "ep:uuid");
        schema.writeAttribute("use", "required");
        schema.writeEndElement(); // attribute

        if (ui->xml_any->isChecked()) {
            // vendor schema extensibility
            schema.writeStartElement("xs:anyAttribute");
            schema.writeAttribute("processContents", "lax");
            schema.writeEndElement();
        }

        schema.writeEndElement(); // complex
        schema.writeEndElement(); // element
    }
}

void MainWindow::insertSchemaUniqueAttributeTypes(QXmlStreamWriter &schema)
{
    insertSchemaUuidAttributeType(schema);
    insertSchemaOrderAttributeType(schema);
}

void MainWindow::insertSchemaVendorObject(QXmlStreamWriter &schema)
{
    //        <xs:element name="VendorObject">
    //            <xs:complexType>
    //              <xs:sequence>
    //                <xs:annotation>
    //                  <xs:appinfo>group: Simulation Parameters</xs:appinfo>
    //                  <xs:documentation xml:lang="en">Open ended, completely flexible object to be used by anyone for any purpose. Inclusion of these objects in the IDFx in any number will not break the validation of the essential IDFx</xs:documentation>
    //                </xs:annotation>
    //                <xs:any processContents="skip" minOccurs="0" maxOccurs="unbounded"/>
    //                <!--allows any number of any xml element to be inserted -->
    //              </xs:sequence>
    //              <xs:attribute name="VendorID" type="xs:string" use="required"/>
    //              <!--required to identify ownership of	object instances encountered-->
    //              <xs:attribute name="ItemID" type="xs:string" use="optional"/>
    //              <!--predefined, though optional, convenience attribute-->
    //              <xs:anyAttribute processContents="lax"/>
    //              <!--allows any xml attribute to be inserted -->
    //            </xs:complexType>
    //          </xs:element>

    schema.writeStartElement("xs:element");
    schema.writeAttribute("name", "VendorObject");
    schema.writeStartElement("xs:complexType");
    schema.writeStartElement("xs:sequence");
    schema.writeStartElement("xs:annotation");
    schema.writeTextElement(XS, "appinfo", "group: Vendor");
    schema.writeTextElement(XS, "documentation", "Open ended, completely flexible object to be used by anyone for any purpose. Inclusion of these objects in the IDFx in any number will not break the validation of the essential IDFx");
    schema.writeEndElement(); // annotation
    schema.writeStartElement("xs:any");
    schema.writeAttribute("processContents", "skip");
    schema.writeAttribute("minOccurs", "0");
    schema.writeAttribute("maxOccurs", "unbounded");
    schema.writeEndElement(); // any
    schema.writeComment("allows any number of any xml element to be inserted");
    schema.writeEndElement(); // sequence
    schema.writeStartElement("xs:attribute");
    schema.writeAttribute("name", "VendorID");
    schema.writeAttribute("type", "xs:string");
    schema.writeAttribute("use", "required");
    schema.writeEndElement(); // attribute
    schema.writeComment("required to identify ownership of VendorObject instances encountered");
    schema.writeStartElement("xs:attribute");
    schema.writeAttribute("name", "ObjectID");
    schema.writeAttribute("type", "xs:string");
    schema.writeAttribute("use", "optional");
    schema.writeEndElement(); // attribute
    schema.writeComment("predefined, though optional, convenience attribute for machine friendly unique object identifier");
    schema.writeStartElement("xs:attribute");
    schema.writeAttribute("name", "ObjectLabel");
    schema.writeAttribute("type", "xs:string");
    schema.writeAttribute("use", "optional");
    schema.writeEndElement(); // attribute
    schema.writeComment("predefined, though optional, convenience attribute for human friendly object display label");
    schema.writeStartElement("xs:anyAttribute");
    schema.writeAttribute("processContents", "lax");
    schema.writeEndElement(); // anyAttribute
    schema.writeComment("allows any vendor xml attribute to be inserted");
    schema.writeEndElement(); // complexType
    schema.writeEndElement(); // element

}

void MainWindow::generateXSDv2() {
    loadSchemaObjectModelFromDB("IDDx");
    // dup db for alteration of new schema
    //(xsd attributes Uuid & Link get added to dbschema)
    // remove deprecated objects/fields

    // alter objects for new schema
    // -name changes
    // -- Name->Label when not 'reference/object-list/choice' type

    // extensible extraction
    // -regular marked extensibles
    // -list like extensibles
    // -node like extensibles

    // add new objects to new schema

    // dump new db to disk

    QFile xsd(QDir::homePath() + "/SCRATCH/IDDx.xsd");
    if (xsd.open(QFile::ReadWrite | QFile::Text | QFile::Truncate)) {
        QXmlStreamWriter schema;
        schema.setDevice(&xsd);
        insertSchemaHeader(schema);
        //        if (DB.isOpen()) {
        QStringList object_names = getSchemaObjectNamesList("IDDx");
        // TODO: add Vertex, and such into the object_names list
        // create those types in IDD?, as they need to be in DB for all other
        // processing
        insertSchemaRootElementList(schema, object_names);
        insertSchemaAllObjectElements(schema);

        insertSchemaUniqueAttributeTypes(schema);
        insertSchemaVendorObject(schema);


        insertSchemaFooter(schema); // element
        //        }  else { //db open
        //            qDebug() << "ERROR: DB is not open - " << DB.lastError();
        //        }
        schema.writeEndDocument();
        xsd.close();
        qDebug() << "DONE writing";
    } else { // xml file
        QString dbg = QString("ERROR: %1 failed to open - %2")
                .arg(xsd.fileName())
                .arg(xsd.errorString());
        qDebug() << dbg;
    }
    ui->xml_xsd_file->setText(
                xsd.fileName()); // this triggers loading view and validating
}

void MainWindow::writeQObjectXML(QXmlStreamWriter &xml, QObject *head)
{
    xml.writeStartElement(EP,head->objectName());
    foreach (QString prop, head->dynamicPropertyNames()) {
        QString val = head->property(prop.toLocal8Bit()).toString();
        if (val != "NOINPUTDATA" && val != ""){
            xml.writeAttribute(EP, prop, val);
        }
    }
    foreach (QObject *child,  head->children()) {
        xml.writeStartElement(EP,child->objectName());
        foreach (QString prop, child->dynamicPropertyNames()) {
            QString val = child->property(prop.toLocal8Bit()).toString();
            if (val != "NOINPUTDATA" && val != ""){
                xml.writeAttribute(EP, prop, val);
            }
        }
        xml.writeEndElement();
    }
    xml.writeEndElement();
}

void MainWindow::generateXMLv2() {
    loadDataObjectModelFromDB("idfx");

    qDebug() << root_data_object->children().count();

    QFile data(QDir::homePath() + "/SCRATCH/IDFx.xml");
    if (data.open(QFile::ReadWrite | QFile::Text | QFile::Truncate)) {
        QXmlStreamWriter xml;
        xml.setDevice(&data);
        xml.setAutoFormatting(true);
        xml.writeStartDocument();
        xml.writeNamespace(EP, "ep");
        xml.writeNamespace(XS, "xs");
        xml.writeDefaultNamespace(EP);

        xml.writeStartElement("idfx");
        foreach (QObject *head, root_data_object->children()) {
            if (head->parent() == root_data_object ) {
                xml.writeStartElement(EP,head->objectName());
                foreach (QString prop, head->dynamicPropertyNames()) {
                    QString val = head->property(prop.toLocal8Bit()).toString();
                    if (val != "EMPTY_FIELD" && val != "" && val != "UNUSED_OPTIONAL_FIELD"){
                        if (val.contains("auto",Qt::CaseInsensitive))
                            val = "-999999";
                        QString ns = ((prop == "uuid") || (prop == "order")) ? EP : "";
                        xml.writeAttribute(ns, prop, val);
                    }
                }
                foreach (QObject *child,  head->children()) {

                    xml.writeStartElement(EP,child->objectName());
                    foreach (QString prop, child->dynamicPropertyNames()) {
                        QString val = child->property(prop.toLocal8Bit()).toString();
                        if (val != "EMPTY_FIELD" && val != "" && val != "UNUSED_OPTIONAL_FIELD"){
                            if (val.contains("auto",Qt::CaseInsensitive))
                                val = "-999999";
                            QString ns = ((prop == "uuid") || (prop == "order")) ? EP : "";
                            xml.writeAttribute(ns, prop, val);
                        }
                    }
                    xml.writeEndElement();
                }
                xml.writeEndElement();
            }
        }
        xml.writeEndElement();

        xml.writeEndDocument();
        data.close();
        qDebug("generateXMLv2 written");
    }
    ui->xml_xml_file->setText(data.fileName());
}

void MainWindow::on_xml_xsd_file_textEdited(const QString &xsd) { // drag/drop

    loadXsdView(xsd);
}

void MainWindow::on_xml_xsd_file_textChanged(const QString &xsd) { // setText
    loadXsdView(xsd);
}

void MainWindow::on_xml_xml_file_textEdited(const QString &xml) {
    loadXmlView(xml);
}

void MainWindow::on_xml_xml_file_textChanged(const QString &xml) {
    loadXmlView(xml);
}

void MainWindow::loadXsdView(const QString &xsd) {
    QString fname(xsd);
    QFile xsdfile(fname.remove("file://"));
    if (xsdfile.open(QFile::ReadOnly | QFile::Text)) {
        ui->xml_xsd->setPlainText(xsdfile.readAll());
        xsdfile.close();
        ui->xml_xsd_file->setText(xsdfile.fileName());
    } else {
        QString dbg = QString("ERROR: %1 failed to open - %2")
                .arg(xsdfile.fileName())
                .arg(xsdfile.errorString());

        ui->statusBar->showMessage(xsdfile.fileName());
    }
}

void MainWindow::loadXmlView(const QString &xml) {
    QString fname(xml);
    QFile xmlfile(fname.remove("file://"));
    if (xmlfile.open(QFile::ReadOnly | QFile::Text)) {
        ui->xml_xml->setPlainText(xmlfile.readAll());
        xmlfile.close();
        ui->xml_xml_file->setText(xmlfile.fileName());
    } else {
        QString dbg = QString("ERROR: %1 failed to open - %2")
                .arg(xmlfile.fileName())
                .arg(xmlfile.errorString());
        ui->statusBar->showMessage(dbg);
    }
}

void MainWindow::on_saveXSD_clicked() {
    QFile savexsd(ui->xml_xsd_file->text().remove("file://"));
    if (savexsd.open(QFile::WriteOnly | QFile::Text | QFile::Truncate)) {
        savexsd.write(ui->xml_xsd->document()->toPlainText().toLocal8Bit());
    } else
        ui->statusBar->showMessage(savexsd.errorString());
    QFile savexml(ui->xml_xml_file->text().remove("file://"));
    if (savexml.open(QFile::WriteOnly | QFile::Text | QFile::Truncate)) {
        savexml.write(ui->xml_xml->document()->toPlainText().toLocal8Bit());
    } else
        emit reportMessage(savexml.errorString());
}

void MainWindow::on_actionDump_Fat_DB_triggered() {
    QString filename =
            QFileDialog::getSaveFileName(this, "dump sqlite file", QDir::homePath());
    Data->saveToDisk(filename);
}

void MainWindow::alterSchema_RemoveObsolete() {
    QString find_obsolete = QString("SELECT * FROM ALTER_select_obsolete;");
    QSqlQuery obsolete_objects = Data->executeQuery(find_obsolete);
    QString action_uuid = Data->getNewUuidString();
    QStringList query_strings(
                getWhyLogEntrySql(action_uuid, "remove obsolete objects"));
    while (obsolete_objects.next()) {
        QString obsolete_object_type =
                obsolete_objects.value("object_type").toString();
        QString obsolete_attribute_type =
                obsolete_objects.value("attribute_type").toString();
        QString obsolete_object_uuid =
                obsolete_objects.value("object_uuid").toString();
        QString obsolete_attribute_uuid =
                obsolete_objects.value("attribute_uuid").toString();
        query_strings.append(
                    QString("DELETE FROM schema_objects WHERE uuid = '%1';")
                    .arg(obsolete_object_uuid));
        query_strings.append(getWhatLogEntrySql(action_uuid, "remove", "object",
                                                obsolete_object_type,
                                                obsolete_object_uuid));
        query_strings.append(
                    QString("DELETE FROM schema_attributes WHERE uuid = '%1';")
                    .arg(obsolete_attribute_uuid));
        query_strings.append(getWhatLogEntrySql(action_uuid, "remove", "attribute",
                                                obsolete_attribute_type,
                                                obsolete_attribute_uuid));
    }
    executeQueryStringList(query_strings);
}

void MainWindow::alterSchema_RemoveDeprecated() {
    QString find_deprecated = QString("SELECT * FROM ALTER_select_deprecated;");
    QSqlQuery deprecated_properties = Data->executeQuery(find_deprecated);
    QString action_uuid = Data->getNewUuidString();
    QStringList query_strings(
                getWhyLogEntrySql(action_uuid, "remove deprecated objects"));
    while (deprecated_properties.next()) {
        QString deprecated_property_type =
                deprecated_properties.value("property_type").toString();
        QString deprecated_attribute_type =
                deprecated_properties.value("attribute_type").toString();
        QString deprecated_property_uuid =
                deprecated_properties.value("property_uuid").toString();
        QString deprecated_attribute_uuid =
                deprecated_properties.value("attribute_uuid").toString();
        query_strings.append(
                    QString("DELETE FROM schema_properties WHERE uuid = '%1';")
                    .arg(deprecated_property_uuid));
        query_strings.append(getWhatLogEntrySql(action_uuid, "remove", "property",
                                                deprecated_property_type,
                                                deprecated_property_uuid));
        query_strings.append(
                    QString("DELETE FROM schema_attributes WHERE uuid = '%1';")
                    .arg(deprecated_attribute_uuid));
        query_strings.append(getWhatLogEntrySql(action_uuid, "remove", "attribute",
                                                deprecated_attribute_type,
                                                deprecated_attribute_uuid));
    }
    executeQueryStringList(query_strings);
}

void MainWindow::executeQueryStringList(QStringList query_strings) {
    Data->executeQuery("BEGIN TRANSACTION;");
    foreach(QString query, query_strings) {
        QSqlQuery qry = Data->executeQuery(query);
        QString qrytxt = qry.lastError().text();
        if (qrytxt != " ")
            qDebug() << qrytxt;
    }
    Data->executeQuery("COMMIT;");
}

void MainWindow::alterSchema_NameToLabel() {
    QString find_name = QString("SELECT * FROM ALTER_select_name;");
    QSqlQuery name_properties = Data->executeQuery(find_name);
    QString action_uuid = Data->getNewUuidString();
    QStringList query_strings(
                getWhyLogEntrySql(action_uuid, "change Name to Label"));
    while (name_properties.next()) {
        QString name_property_type =
                name_properties.value("property_type").toString();
        QString name_property_uuid =
                name_properties.value("property_uuid").toString();

        query_strings.append(
                    QString("UPDATE schema_properties SET property_type = 'Label' WHERE "
                            "uuid = '%1';").arg(name_property_uuid));
        query_strings.append(getWhatLogEntrySql(
                                 action_uuid, "change", "property", name_property_type,
                                 name_property_uuid, name_property_type, "Label"));
    }
    executeQueryStringList(query_strings);

    // these alterSchema inserted here, as they depend on application of
    // alterSchema_NameToLabel
    alterSchema_DeprecateRetaincase();
    alterSchema_DeprecateRequiredField();
    alterSchema_SetLabelDataType();
}

void MainWindow::alterschema_PropertyName(QSqlQuery props, QStringList &sql_strings, QString badstring, QString goodstring)
{
    QString prop_uuid = props.value("uuid").toString();
    QString old_type = props.value("property_type").toString();
    QString new_type = old_type;
    new_type.replace(badstring,goodstring);
    sql_strings.append(QString("UPDATE schema_properties SET property_type = '%1' where uuid = '%2';")
                       .arg(new_type).arg(prop_uuid));
    sql_strings.append(getWhatLogEntrySql(prop_uuid, "change", "property",
                                          old_type, new_type));
}

void MainWindow::alterSchema_IllegalCharacterFix()
{
    QString action_uuid = Data->getNewUuidString();
    QStringList query_strings(
                getWhyLogEntrySql(action_uuid, "replace invalid characters",
                                  "some characters are invalid/problematic for property or attribute names"));
    //properties
    QString old_string = ":";
    QString new_string = "-";
    QSqlQuery props1 = Data->executeQuery(
                QString("select uuid, property_type from schema_properties where property_type like '\%%1\%';")
                .arg(old_string));
    while (props1.next()) {
        alterschema_PropertyName(props1, query_strings, old_string, new_string);
    }
    executeQueryStringList(query_strings);
    query_strings.clear();
    old_string = " ";
    new_string = "_";
    QSqlQuery props4 = Data->executeQuery(
                QString("select uuid, property_type from schema_properties where property_type like '\%%1\%';")
                .arg(old_string));
    while (props4.next()) {
        alterschema_PropertyName(props4, query_strings, old_string, new_string);
    }
    executeQueryStringList(query_strings);
    query_strings.clear();

    old_string = "(";
    new_string = "-";
    QSqlQuery props5 = Data->executeQuery(
                QString("select uuid, property_type from schema_properties where property_type like '\%%1\%';")
                .arg(old_string));
    while (props5.next()) {
        alterschema_PropertyName(props5, query_strings, old_string, new_string);
    }
    executeQueryStringList(query_strings);
    query_strings.clear();

    old_string = ")";
    new_string = "-";
    QSqlQuery props6 = Data->executeQuery(
                QString("select uuid, property_type from schema_properties where property_type like '\%%1\%';")
                .arg(old_string));
    while (props6.next()) {
        alterschema_PropertyName(props6, query_strings, old_string, new_string);
    }
    executeQueryStringList(query_strings);
    query_strings.clear();

    old_string = "/";
    new_string = "-";
    QSqlQuery props7 = Data->executeQuery(
                QString("select uuid, property_type from schema_properties where property_type like '\%%1\%';")
                .arg(old_string));
    while (props7.next()) {
        alterschema_PropertyName(props7, query_strings, old_string, new_string);
    }
    executeQueryStringList(query_strings);
    query_strings.clear();

    old_string = "*";
    new_string = "x";
    QSqlQuery props8 = Data->executeQuery(
                QString("select uuid, property_type from schema_properties where property_type like '\%%1\%';")
                .arg(old_string));
    while (props8.next()) {
        alterschema_PropertyName(props8, query_strings, old_string, new_string);
    }
    executeQueryStringList(query_strings);
    query_strings.clear();

    old_string = "?";
    new_string = "";
    QSqlQuery props9 = Data->executeQuery(
                QString("select uuid, property_type from schema_properties where property_type like '\%%1\%';")
                .arg(old_string));
    while (props9.next()) {
        alterschema_PropertyName(props9, query_strings, old_string, new_string);
    }
    executeQueryStringList(query_strings);
    query_strings.clear();

    old_string = ",";
    new_string = "";
    QSqlQuery props10 = Data->executeQuery(
                QString("select uuid, property_type from schema_properties where property_type like '\%%1\%';")
                .arg(old_string));
    while (props10.next()) {
        alterschema_PropertyName(props10, query_strings, old_string, new_string);
    }
    executeQueryStringList(query_strings);
    query_strings.clear();

    old_string = "]";
    new_string = "";
    QSqlQuery props11 = Data->executeQuery(
                QString("select uuid, property_type from schema_properties where property_type like '\%%1\%';")
                .arg(old_string));
    while (props11.next()) {
        alterschema_PropertyName(props11, query_strings, old_string, new_string);
    }
    executeQueryStringList(query_strings);
    query_strings.clear();

    old_string = "=";
    new_string = "";
    QSqlQuery props12 = Data->executeQuery(
                QString("select uuid, property_type from schema_properties where property_type like '\%%1\%';")
                .arg(old_string));
    while (props12.next()) {
        alterschema_PropertyName(props12, query_strings, old_string, new_string);
    }
    executeQueryStringList(query_strings);
    query_strings.clear();

    old_string = "+";
    new_string = "";
    QSqlQuery props13 = Data->executeQuery(
                QString("select uuid, property_type from schema_properties where property_type like '\%%1\%';")
                .arg(old_string));
    while (props13.next()) {
        alterschema_PropertyName(props13, query_strings, old_string, new_string);
    }
    executeQueryStringList(query_strings);
    query_strings.clear();

    old_string = "%";
    new_string = "pct";
    QSqlQuery props14 = Data->executeQuery(
                QString("select uuid, property_type from schema_properties where property_type like '\%%1\%';")
                .arg(old_string));
    while (props14.next()) {
        alterschema_PropertyName(props14, query_strings, old_string, new_string);
    }
    executeQueryStringList(query_strings);
    query_strings.clear();

    //objects
    old_string = ":";
    new_string = "-";
    QSqlQuery objs = Data->executeQuery("select * from schema_objects where object_type like '%:%';");
    while (objs.next()) {
        QString obj_uuid = objs.value("uuid").toString();
        QString old_type = objs.value("object_type").toString();
        QString new_type = old_type;
        new_type.replace(old_string,new_string);
        query_strings.append(QString("UPDATE schema_objects SET object_type = '%1' where uuid = '%2';")
                             .arg(new_type).arg(obj_uuid));
        query_strings.append(getWhatLogEntrySql(obj_uuid, "change", "object",
                                                old_type, new_type));
    }
    executeQueryStringList(query_strings);
}

void MainWindow::alterSchema_ObjectListEnumerations() {
    QString action_uuid = Data->getNewUuidString();
    QStringList query_strings(
                getWhyLogEntrySql(action_uuid, "replace colons in object types listed in enumerations",
                                  "so that the object type strings will match new objects when using enumerations"));
    QString old_string = ":";
    QString new_string = "-";
    QSqlQuery objs = Data->executeQuery("select * from ALTER_object_list_enumerations;");
    while (objs.next()) {
        QString obj_uuid = objs.value("attribute_uuid").toString();
        QString old_value = objs.value("attribute_value").toString();
        QString new_value = old_value;
        new_value.replace(old_string,new_string);
        QString update = QString("UPDATE schema_attributes SET attribute_value = '%1' where uuid = '%2';")
                .arg(new_value).arg(obj_uuid);
        query_strings.append(update);
        query_strings.append(getWhatLogEntrySql(obj_uuid, "change", "attribute",
                                                old_value, new_value));
    }
    executeQueryStringList(query_strings);

}

void MainWindow::alterSchema_DeprecateRetaincase() {
    QString action_uuid = Data->getNewUuidString();
    QStringList query_strings(
                getWhyLogEntrySql(action_uuid, "delete Retaincase attribute from Label",
                                  "Label will always retain its case"));
    QSqlQuery changeset = Data->executeQuery(
                "SELECT * FROM ALTER_deprecate_label_attribute_retaincase;");
    while (changeset.next()) {
        QString delete_uuid = changeset.value("attribute_uuid").toString();
        query_strings.append(
                    QString("delete from schema_attributes where uuid = '%1';")
                    .arg(delete_uuid));
        query_strings.append(getWhatLogEntrySql(action_uuid, "delete", "attribute",
                                                "retaincase", delete_uuid));
    }
    executeQueryStringList(query_strings);
}

void MainWindow::alterSchema_DeprecateRequiredField() {
    QString action_uuid = Data->getNewUuidString();
    QStringList query_strings(getWhyLogEntrySql(
                                  action_uuid, "delete RequiredField attribute from Label",
                                  "Labels are always optional"));
    QSqlQuery changeset = Data->executeQuery(
                "SELECT * FROM ALTER_deprecate_label_attribute_required_field;");
    while (changeset.next()) {
        QString delete_uuid = changeset.value("attribute_uuid").toString();
        query_strings.append(
                    QString("delete from schema_attributes where uuid = '%1';")
                    .arg(delete_uuid));
        query_strings.append(getWhatLogEntrySql(action_uuid, "delete", "attribute",
                                                "required_field", delete_uuid));
    }
    executeQueryStringList(query_strings);
}

void MainWindow::alterSchema_SetLabelDataType() {
    QString action_uuid = Data->getNewUuidString();
    QStringList query_strings(getWhyLogEntrySql(
                                  action_uuid, "set Label datatype to alpha, where it is currently blank"));
    QSqlQuery changeset =
            Data->executeQuery("SELECT * FROM ALTER_label_without_datatype;");
    while (changeset.next()) {
        QString affected_uuid = changeset.value("property_uuid").toString();
        QString old_datatype = changeset.value("data_type").toString();
        query_strings.append(
                    QString("UPDATE schema_properties SET data_type = 'alpha' WHERE uuid = "
                            "'%1';").arg(affected_uuid));
        query_strings.append(getWhatLogEntrySql(action_uuid, "change", "property",
                                                "data_type", affected_uuid,
                                                old_datatype, "alpha"));
    }
    executeQueryStringList(query_strings);
}

void MainWindow::alterSchema_DataTypeCase() {
    QString action_uuid = Data->getNewUuidString();
    QStringList query_strings(
                getWhyLogEntrySql(action_uuid, "lower case alpha and choice datatypes"));
    QSqlQuery changeset =
            Data->executeQuery("SELECT * FROM ALTER_normalize_datatype_alpha;");
    while (changeset.next()) {
        QString affected_uuid = changeset.value("property_uuid").toString();
        QString old_datatype = changeset.value("data_type").toString();
        query_strings.append(
                    QString("UPDATE schema_properties SET data_type = 'alpha' WHERE uuid = "
                            "'%1';").arg(affected_uuid));
        query_strings.append(getWhatLogEntrySql(action_uuid, "change", "property",
                                                "data_type", affected_uuid,
                                                old_datatype, "alpha"));
    }
    QSqlQuery changeset2 =
            Data->executeQuery("SELECT * FROM ALTER_normalize_datatype_choice;");
    while (changeset2.next()) {
        QString affected_uuid = changeset2.value("property_uuid").toString();
        QString old_datatype = changeset2.value("data_type").toString();
        query_strings.append(
                    QString("UPDATE schema_properties SET data_type = 'choice' WHERE uuid "
                            "= '%1';").arg(affected_uuid));
        query_strings.append(getWhatLogEntrySql(action_uuid, "change", "property",
                                                "data_type", affected_uuid,
                                                old_datatype, "choice"));
    }
    executeQueryStringList(query_strings);
}

void MainWindow::alterSchema_AlphaDataTypeSpecifierA() {
    QString action_uuid = Data->getNewUuidString();
    QStringList query_strings(getWhyLogEntrySql(
                                  action_uuid, "set field datatype to alpha, where it is currently blank "
                                               "and it has an A# specifier"));
    QSqlQuery changeset =
            Data->executeQuery("SELECT * FROM ALTER_blank_datatype_specifier_A;");
    while (changeset.next()) {
        QString affected_uuid = changeset.value("property_uuid").toString();
        QString old_datatype = changeset.value("data_type").toString();
        query_strings.append(
                    QString("UPDATE schema_properties SET data_type = 'alpha' WHERE uuid = "
                            "'%1';").arg(affected_uuid));
        query_strings.append(QString("INSERT INTO schema_attributes VALUES( '%1', '%2', 'data_type', 'alpha');")
                             .arg(Data->getNewUuidString()).arg(affected_uuid));
        query_strings.append(getWhatLogEntrySql(action_uuid, "change", "property",
                                                "data_type", affected_uuid,
                                                old_datatype, "alpha"));
        query_strings.append(getWhatLogEntrySql(action_uuid, "add", "attribute",
                                                "data_type", affected_uuid,
                                                "", "alpha"));
    }
    executeQueryStringList(query_strings);
}

void MainWindow::alterSchema_SetDataTypeRealByValues() {
    QString action_uuid = Data->getNewUuidString();
    QStringList query_strings(getWhyLogEntrySql(
                                  action_uuid, "set field data_type to real based on values",
                                  "based on min/max/default values written as a real (with decimal - #.#"));
    QSqlQuery changeset1 = Data->executeQuery(
                "SELECT * FROM ALTER_set_decimal_minimum_datatype_real;");
    while (changeset1.next()) {
        QString affected_uuid = changeset1.value("property_uuid").toString();
        query_strings.append(
                    QString("UPDATE schema_properties SET data_type = 'real' WHERE uuid = "
                            "'%1';").arg(affected_uuid));
        query_strings.append(getWhatLogEntrySql(action_uuid, "change", "property",
                                                "data_type", affected_uuid, "",
                                                "real"));
    }
    QSqlQuery changeset2 = Data->executeQuery(
                "SELECT * FROM ALTER_set_decimal_maximum_datatype_real;");
    while (changeset2.next()) {
        QString affected_uuid = changeset2.value("property_uuid").toString();
        query_strings.append(
                    QString("UPDATE schema_properties SET data_type = 'real' WHERE uuid = "
                            "'%1';").arg(affected_uuid));
        query_strings.append(getWhatLogEntrySql(action_uuid, "change", "property",
                                                "data_type", affected_uuid, "",
                                                "real"));
    }
    QSqlQuery changeset3 = Data->executeQuery(
                "SELECT * FROM ALTER_set_decimal_ex_minimum_datatype_real;");
    while (changeset3.next()) {
        QString affected_uuid = changeset3.value("property_uuid").toString();
        query_strings.append(
                    QString("UPDATE schema_properties SET data_type = 'real' WHERE uuid = "
                            "'%1';").arg(affected_uuid));
        query_strings.append(getWhatLogEntrySql(action_uuid, "change", "property",
                                                "data_type", affected_uuid, "",
                                                "real"));
    }
    QSqlQuery changeset4 = Data->executeQuery(
                "SELECT * FROM ALTER_set_decimal_ex_maximum_datatype_real;");
    while (changeset4.next()) {
        QString affected_uuid = changeset4.value("property_uuid").toString();
        query_strings.append(
                    QString("UPDATE schema_properties SET data_type = 'real' WHERE uuid = "
                            "'%1';").arg(affected_uuid));
        query_strings.append(getWhatLogEntrySql(action_uuid, "change", "property",
                                                "data_type", affected_uuid, "",
                                                "real"));
    }
    QSqlQuery changeset5 = Data->executeQuery(
                "SELECT * FROM ALTER_set_decimal_default_datatype_real;");
    while (changeset5.next()) {
        QString affected_uuid = changeset5.value("property_uuid").toString();
        query_strings.append(
                    QString("UPDATE schema_properties SET data_type = 'real' WHERE uuid = "
                            "'%1';").arg(affected_uuid));
        query_strings.append(getWhatLogEntrySql(action_uuid, "change", "property",
                                                "data_type", affected_uuid, "",
                                                "real"));
    }
    executeQueryStringList(query_strings);
}

void MainWindow::alterSchema_SetDataTypeIntegerByValues() {
    QString action_uuid = Data->getNewUuidString();
    QStringList query_strings(getWhyLogEntrySql(
                                  action_uuid, "set field data_type to integer based on values",
                                  "based on min/max/default values written as a integer (withOUT decimal - "
                                  "#.#"));
    QSqlQuery changeset1 = Data->executeQuery(
                "SELECT * FROM ALTER_set_not_decimal_minimum_datatype_integer;");
    while (changeset1.next()) {
        QString affected_uuid = changeset1.value("property_uuid").toString();
        query_strings.append(
                    QString("UPDATE schema_properties SET data_type = 'integer' WHERE uuid "
                            "= '%1';").arg(affected_uuid));
        query_strings.append(getWhatLogEntrySql(action_uuid, "change", "property",
                                                "data_type", affected_uuid, "",
                                                "integer"));
    }
    QSqlQuery changeset2 = Data->executeQuery(
                "SELECT * FROM ALTER_set_not_decimal_maximum_datatype_integer;");
    while (changeset2.next()) {
        QString affected_uuid = changeset2.value("property_uuid").toString();
        query_strings.append(
                    QString("UPDATE schema_properties SET data_type = 'integer' WHERE uuid "
                            "= '%1';").arg(affected_uuid));
        query_strings.append(getWhatLogEntrySql(action_uuid, "change", "property",
                                                "data_type", affected_uuid, "",
                                                "integer"));
    }
    QSqlQuery changeset3 = Data->executeQuery(
                "SELECT * FROM ALTER_set_not_decimal_ex_minimum_datatype_integer;");
    while (changeset3.next()) {
        QString affected_uuid = changeset3.value("property_uuid").toString();
        query_strings.append(
                    QString("UPDATE schema_properties SET data_type = 'integer' WHERE uuid "
                            "= '%1';").arg(affected_uuid));
        query_strings.append(getWhatLogEntrySql(action_uuid, "change", "property",
                                                "data_type", affected_uuid, "",
                                                "integer"));
    }
    QSqlQuery changeset4 = Data->executeQuery(
                "SELECT * FROM ALTER_set_not_decimal_ex_maximum_datatype_integer;");
    while (changeset4.next()) {
        QString affected_uuid = changeset4.value("property_uuid").toString();
        query_strings.append(
                    QString("UPDATE schema_properties SET data_type = 'integer' WHERE uuid "
                            "= '%1';").arg(affected_uuid));
        query_strings.append(getWhatLogEntrySql(action_uuid, "change", "property",
                                                "data_type", affected_uuid, "",
                                                "integer"));
    }
    QSqlQuery changeset5 = Data->executeQuery(
                "SELECT * FROM ALTER_set_not_decimal_default_datatype_integer;");
    while (changeset5.next()) {
        QString affected_uuid = changeset5.value("property_uuid").toString();
        query_strings.append(
                    QString("UPDATE schema_properties SET data_type = 'integer' WHERE uuid "
                            "= '%1';").arg(affected_uuid));
        query_strings.append(getWhatLogEntrySql(action_uuid, "change", "property",
                                                "data_type", affected_uuid, "",
                                                "integer"));
    }
    executeQueryStringList(query_strings);
}

void MainWindow::alterSchema_SetDataTypeNameInference() {
    QString action_uuid = Data->getNewUuidString();
    QStringList query_strings(getWhyLogEntrySql(
                                  action_uuid, "set field data_type based on name inference",
                                  "remaining fields with names ending in Year  or including Priority "
                                  "or of the Output:DebuggingData object are set to integer.  all "
                                  "remaining fields have names including coefficient, factor, "
                                  "efficiency or the like and are set to datatype real "));
    QSqlQuery changeset1 =
            Data->executeQuery("SELECT * FROM ALTER_set_integer_datatype_priority;");
    while (changeset1.next()) {
        QString affected_uuid = changeset1.value("property_uuid").toString();
        query_strings.append(
                    QString("UPDATE schema_properties SET data_type = 'integer' WHERE uuid "
                            "= '%1';").arg(affected_uuid));
        query_strings.append(getWhatLogEntrySql(action_uuid, "change", "property",
                                                "data_type", affected_uuid, "",
                                                "integer"));
    }
    QSqlQuery changeset2 =
            Data->executeQuery("SELECT * FROM ALTER_set_integer_datatype_year;");
    while (changeset2.next()) {
        QString affected_uuid = changeset2.value("property_uuid").toString();
        query_strings.append(
                    QString("UPDATE schema_properties SET data_type = 'integer' WHERE uuid "
                            "= '%1';").arg(affected_uuid));
        query_strings.append(getWhatLogEntrySql(action_uuid, "change", "property",
                                                "data_type", affected_uuid, "",
                                                "integer"));
    }
    QSqlQuery changeset3 =
            Data->executeQuery("SELECT * FROM ALTER_set_integer_datatype_debugdata;");
    while (changeset3.next()) {
        QString affected_uuid = changeset3.value("property_uuid").toString();
        query_strings.append(
                    QString("UPDATE schema_properties SET data_type = 'integer' WHERE uuid "
                            "= '%1';").arg(affected_uuid));
        query_strings.append(getWhatLogEntrySql(action_uuid, "change", "property",
                                                "data_type", affected_uuid, "",
                                                "integer"));
    }
    QSqlQuery changeset5 =
            Data->executeQuery("SELECT * FROM 'schema_object_properties_attributes' "
                               "where data_type = ''");
    while (changeset5.next()) {
        QString affected_uuid = changeset5.value("property_uuid").toString();
        query_strings.append(
                    QString("UPDATE schema_properties SET data_type = 'real' WHERE uuid = "
                            "'%1';").arg(affected_uuid));
        query_strings.append(getWhatLogEntrySql(action_uuid, "change", "property",
                                                "data_type", affected_uuid, "",
                                                "real"));
    }
    executeQueryStringList(query_strings);
}

void MainWindow::alterSchema_EveryObjectHasNewUuidField() {
    // TODO: log that EveryObjectHasNewUuidField(), or some reasonable facsimile
}

void MainWindow::alterSchema_SetMissingUnitDataTypes() {
    QString action_uuid = Data->getNewUuidString();
    QStringList query_strings(getWhyLogEntrySql(
                                  action_uuid, "set data_type on fields with units",
                                  "set fields with no data_type to real or integer based on units "
                                  "attribute.  in particular hr, days set to integer, and also set the "
                                  "one with hh:mm to alpha"));
    QSqlQuery changeset1 =
            Data->executeQuery("SELECT * FROM ALTER_set_unit_datatype_real;");
    while (changeset1.next()) {
        QString affected_uuid = changeset1.value("property_uuid").toString();
        query_strings.append(
                    QString("UPDATE schema_properties SET data_type = 'real' WHERE uuid = "
                            "'%1';").arg(affected_uuid));
        query_strings.append(getWhatLogEntrySql(action_uuid, "change", "property",
                                                "data_type", affected_uuid, "",
                                                "real"));
    }
    QSqlQuery changeset2 =
            Data->executeQuery("SELECT * FROM ALTER_set_unit_datatype_integer;");
    while (changeset2.next()) {
        QString affected_uuid = changeset2.value("property_uuid").toString();
        query_strings.append(
                    QString("UPDATE schema_properties SET data_type = 'integer' WHERE uuid "
                            "= '%1';").arg(affected_uuid));
        query_strings.append(getWhatLogEntrySql(action_uuid, "change", "property",
                                                "data_type", affected_uuid, "",
                                                "integer"));
    }
    QSqlQuery changeset3 =
            Data->executeQuery("SELECT * FROM ALTER_set_unit_datatype_alpha;");
    while (changeset3.next()) {
        QString affected_uuid = changeset3.value("property_uuid").toString();
        query_strings.append(
                    QString("UPDATE schema_properties SET data_type = 'alpha' WHERE uuid = "
                            "'%1';").arg(affected_uuid));
        query_strings.append(getWhatLogEntrySql(action_uuid, "change", "property",
                                                "data_type", affected_uuid, "",
                                                "alpha"));
    }
    executeQueryStringList(query_strings);
}

void MainWindow::alterSchema_SetDataTypeObjectList() {
    QString action_uuid = Data->getNewUuidString();
    QStringList query_strings(getWhyLogEntrySql(
                                  action_uuid,
                                  "set data_type to object_list of Fields with attribute object-list",
                                  "some Field data_types are blank, some alpha, some spelled with "
                                  "irregular case as Object-type"));
    QSqlQuery changeset =
            Data->executeQuery("SELECT * FROM ALTER_alpha_datatype_objectlist;");
    while (changeset.next()) {
        QString affected_uuid = changeset.value("property_uuid").toString();
        query_strings.append(
                    QString("UPDATE schema_properties SET data_type = 'object_list' WHERE "
                            "uuid = '%1';").arg(affected_uuid));
        query_strings.append(getWhatLogEntrySql(action_uuid, "change", "property",
                                                "data_type", affected_uuid, "alpha",
                                                "object_list"));
    }
    QSqlQuery changeset2 =
            Data->executeQuery("SELECT * FROM ALTER_blank_datatype_objectlist;");
    while (changeset2.next()) {
        QString affected_uuid = changeset2.value("property_uuid").toString();
        query_strings.append(
                    QString("UPDATE schema_properties SET data_type = 'object_list' WHERE "
                            "uuid = '%1';").arg(affected_uuid));
        query_strings.append(getWhatLogEntrySql(action_uuid, "change", "property",
                                                "data_type", affected_uuid, "",
                                                "object_list"));
    }
    QSqlQuery changeset3 =
            Data->executeQuery("SELECT * FROM ALTER_normalize_datatype_objectlist;");
    while (changeset3.next()) {
        QString affected_uuid = changeset3.value("property_uuid").toString();
        QString old_datatype = changeset3.value("data_type").toString();
        query_strings.append(
                    QString("UPDATE schema_properties SET data_type = 'object_list' WHERE "
                            "uuid = '%1';").arg(affected_uuid));
        query_strings.append(getWhatLogEntrySql(action_uuid, "change", "property",
                                                "data_type", affected_uuid,
                                                old_datatype, "object_list"));
    }
    QSqlQuery changeset4 =
            Data->executeQuery("SELECT * FROM ALTER_more_normalize_datatype_objectlist;");
    while (changeset4.next()) {
        QString attribute_uuid = changeset4.value("attribute_uuid").toString();
        QString attribute_value = changeset4.value("attribute_value").toString();
        query_strings.append(
                    QString("UPDATE schema_attributes SET attribute_value = 'object_list' WHERE "
                            "uuid = '%1';").arg(attribute_uuid));
        query_strings.append(getWhatLogEntrySql(action_uuid, "change", "attribute",
                                                "attribute_value", attribute_uuid,
                                                attribute_value, "object_list"));
    }
    executeQueryStringList(query_strings);
}

void MainWindow::alterSchema_SetDataTypeExternalList() {
    QString action_uuid = Data->getNewUuidString();
    QStringList query_strings(getWhyLogEntrySql(
                                  action_uuid, "set data_type of external-list to external_list",
                                  "to normalize processing and avoid misinterpretation"));
    QSqlQuery propertyset =
            Data->executeQuery("SELECT * FROM ALTER_select_properties_external_list;");
    while (propertyset.next()) {
        QString affected_uuid = propertyset.value("uuid").toString();
        query_strings.append(
                    QString("UPDATE schema_properties SET data_type = 'external_list' "
                            "WHERE uuid = '%1';").arg(affected_uuid));
        query_strings.append(getWhatLogEntrySql(action_uuid, "change", "property",
                                                "data_type", affected_uuid,
                                                "external-list", "external_list"));
    }
    QSqlQuery attributeset =
            Data->executeQuery("SELECT * FROM ALTER_select_attributes_external_list;");
    while (attributeset.next()) {
        QString affected_uuid = attributeset.value("uuid").toString();
        query_strings.append(
                    QString("UPDATE schema_attributes SET attribute_value = 'external_list' "
                            "WHERE uuid = '%1';").arg(affected_uuid));
        query_strings.append(getWhatLogEntrySql(action_uuid, "change", "attribute",
                                                "attribute_value", affected_uuid,
                                                "external-list", "external_list"));
    }
    executeQueryStringList(query_strings);
}

void MainWindow::alterSchema_SetOptionalRequired()
{
    QString action_uuid = Data->getNewUuidString();
    QStringList query_strings(getWhyLogEntrySql(
                                  action_uuid, "setting properties required/optional",
                                  "heuristically infer from \notes if a property is optional"));
    //TODO: mark all fields required, except these... so, mark all these 'optional', then mark remaining 'required'

    //    select subject_uuid from schema_attributes where attribute_type = 'note' and attribute_value like '%required%if%';
    //    select subject_uuid from schema_attributes where attribute_type = 'note' and attribute_value like '%required%when%';
    //    select subject_uuid from schema_attributes where attribute_type = 'note' and attribute_value like '%required%only%';
    //    select subject_uuid from schema_attributes where attribute_type = 'note' and attribute_value like '%optional%';
    QSqlQuery optional_properties_qry = Data->executeQuery("select subject_uuid from schema_attributes where attribute_type = 'note' and attribute_value like '%optional%';");
    QString insert_str;
    while (optional_properties_qry.next()) {
        QString subject_uuid = optional_properties_qry.value("subject_uuid").toString();
        //TODO:log this
        insert_str = QString("INSERT INTO schema_attributes VALUES ('%1', '%2', '%3', '%4');")
                .arg(Data->getNewUuidString())
                .arg(subject_uuid)
                .arg("required_field")
                .arg("False");
        query_strings.append(insert_str);
    }

    QSqlQuery when_properties_qry = Data->executeQuery("select subject_uuid from schema_attributes where attribute_type = 'note' and attribute_value like '%required%when%';");
    insert_str.clear();
    while (when_properties_qry.next()) {
        QString subject_uuid = when_properties_qry.value("subject_uuid").toString();
        //TODO:log this
        insert_str = QString("INSERT INTO schema_attributes VALUES ('%1', '%2', '%3', '%4');")
                .arg(Data->getNewUuidString())
                .arg(subject_uuid)
                .arg("required_field")
                .arg("False");
        query_strings.append(insert_str);
    }

    QSqlQuery only_properties_qry = Data->executeQuery("select subject_uuid from schema_attributes where attribute_type = 'note' and attribute_value like '%required%only%';");
    insert_str.clear();
    while (only_properties_qry.next()) {
        QString subject_uuid = only_properties_qry.value("subject_uuid").toString();
        //TODO:log this
        insert_str = QString("INSERT INTO schema_attributes VALUES ('%1', '%2', '%3', '%4');")
                .arg(Data->getNewUuidString())
                .arg(subject_uuid)
                .arg("required_field")
                .arg("False");
        query_strings.append(insert_str);
    }

    QSqlQuery if_properties_qry = Data->executeQuery("select subject_uuid from schema_attributes where attribute_type = 'note' and attribute_value like '%required%if%';");
    insert_str.clear();
    while (if_properties_qry.next()) {
        QString subject_uuid = if_properties_qry.value("subject_uuid").toString();
        //TODO:log this
        insert_str = QString("INSERT INTO schema_attributes VALUES ('%1', '%2', '%3', '%4');")
                .arg(Data->getNewUuidString())
                .arg(subject_uuid)
                .arg("required_field")
                .arg("False");
        query_strings.append(insert_str);
    }

    QSqlQuery remaining_properties_qry = Data->executeQuery("select uuid from schema_properties where uuid not in (select property_uuid from schema_object_properties_required_field);");
    insert_str.clear();
    while (remaining_properties_qry.next()) {
        QString subject_uuid = remaining_properties_qry.value("uuid").toString();
        //TODO:log this
        insert_str = QString("INSERT INTO schema_attributes VALUES ('%1', '%2', '%3', '%4');")
                .arg(Data->getNewUuidString())
                .arg(subject_uuid)
                .arg("required_field")
                .arg("True");
        query_strings.append(insert_str);
    }
    executeQueryStringList(query_strings);
}

void MainWindow::alterSchema_EnsureAttributeDataTypeEntries()
{
//    QSqlQuery changeset = Data->executeQuery("SELECT uuid, data_type FROM schema_properties;");
//    while (changeset.next()) {
//        QString prop_uuid = changeset.value("uuid").toString();
//        QString prop_datatype = changeset.value("data_type").toString();
//        QString update_qry = QString("UPDATE schema_attributes SET subject_uuid = '%1', attribute_type = '%3', attribute_value = '%4'").arg(prop_uuid).arg("data_type").arg(prop_datatype);
//        QSqlQuery changeset = Data->executeQuery(update_qry);
//        qDebug() << "alterSchema_EnsureAttributeDataTypeEntries" << changeset.lastError();
//    }
}

void MainWindow::alterSchema_ExtensibleExtraction()
{
    //first, mark additional objects/properties as extensible
    alterSchema_MarkEnumerationsExtensible();

    //get extensible, foreach make new Object and re-assign begin-extensible through extensible# to new Object
    //reset object attribute extensible:# to 0?
    //remove begin-extensible attribute
    QString action_uuid = Data->getNewUuidString();
    QStringList query_strings(getWhyLogEntrySql(
                                  action_uuid, "create extensible types for objects marked extensible",
                                  "for each extensible object, take a single instances fields after (and including) the field marked begin-extensible"));
    QSqlQuery changeset = Data->executeQuery("SELECT * FROM ALTER_extensible_with_begin_extensible;");
    //extensible objects
    while (changeset.next()) {
        QString old_object_uuid = changeset.value("object_uuid").toString();
        QString old_object_type = changeset.value("object_type").toString();
        //insert _extension type object
        QString new_object_uuid = Data->getNewUuidString();
        QString new_object_type = QString("%1_x").arg(old_object_type);
        query_strings.append(QString("INSERT INTO schema_objects VALUES ('%1','%2','%3');")
                             .arg(new_object_uuid)
                             .arg(new_object_type)
                             .arg(""));
        query_strings.append(getWhatLogEntrySql(action_uuid, "add", "object",
                                                new_object_type, new_object_uuid));
        //insert new _extension object \memo
        QString new_attribute_uuid = Data->getNewUuidString();
        query_strings.append(QString("INSERT INTO schema_attributes VALUES ('%1','%2','%3','%4');")
                             .arg(new_attribute_uuid)
                             .arg(new_object_uuid)
                             .arg("memo")
                             .arg("New object containing the extensible fields of the original object"));
        query_strings.append(getWhatLogEntrySql(action_uuid, "add", "attribute",
                                                "memo", new_attribute_uuid));
        //add \order property for _extension objects that may need it
        //        QString new_order_property_uuid = Data->getNewUuidString();
        //        query_strings.append(QString("INSERT INTO schema_properties VALUES ('%1','%2','%3','%4');")
        //                             .arg(new_order_property_uuid)
        //                             .arg(new_object_uuid)
        //                             .arg("order")
        //                             .arg("integer"));
        //        query_strings.append(getWhatLogEntrySql(action_uuid, "add", "property", "order", ""));
        //        //set the data_type for the \order property to integer
        //        QString new_order_datatype_attribute_uuid = Data->getNewUuidString();
        //        query_strings.append(QString("INSERT INTO schema_attributes VALUES ('%1','%2','%3','%4');")
        //                             .arg(new_order_datatype_attribute_uuid)
        //                             .arg(new_order_property_uuid)
        //                             .arg("data_type")
        //                             .arg("integer"));
        //        query_strings.append(getWhatLogEntrySql(action_uuid, "add", "attribute",
        //                                                "data_type", new_order_datatype_attribute_uuid));
        //        //set minimum for the \order property to 0
        //        QString new_order_minval_attribute_uuid = Data->getNewUuidString();
        //        query_strings.append(QString("INSERT INTO schema_attributes VALUES ('%1','%2','%3','%4');")
        //                             .arg(new_order_minval_attribute_uuid)
        //                             .arg(new_order_property_uuid)
        //                             .arg("minimum_value")
        //                             .arg("0"));
        //        query_strings.append(getWhatLogEntrySql(action_uuid, "add", "attribute",
        //                                                "minimum", new_order_minval_attribute_uuid));
        //begin-extensible
        QSqlQuery begin_extensible_qry = Data->executeQuery(QString("SELECT p.object_type, p.property_type, p.position, e.attribute_type, e.attribute_value, e.attribute_uuid "
                                                                    "FROM schema_object_properties_position p, ALTER_begin_extensible e "
                                                                    "WHERE e.attribute_type = 'begin_extensible' "
                                                                    "AND  e.property_uuid = p.property_uuid "
                                                                    "AND  e.object_uuid = p.object_uuid "
                                                                    "AND  p.object_uuid = '%1';")
                                                            .arg(old_object_uuid));
        begin_extensible_qry.first();
        int begin_postion = begin_extensible_qry.value("p.position").toInt(); //yes, these values could all come from one beautiful query, just not now
        QSqlQuery extension_value_qry = Data->executeQuery(QString("SELECT * FROM 'ALTER_select_extensible' "
                                                                   "WHERE object_uuid = '%1'; ")
                                                           .arg(old_object_uuid));
        extension_value_qry.first();
        int extensible_value = extension_value_qry.value("attribute_value").toInt();
        int end_position = extensible_value + begin_postion - 1;
        QSqlQuery extension_properties_set = Data->executeQuery(QString("SELECT * FROM 'schema_object_properties_position' "
                                                                        "WHERE position >= %1 "
                                                                        "AND  position <= %2 "
                                                                        "AND object_uuid = '%3';")
                                                                .arg(begin_postion)
                                                                .arg(end_position)
                                                                .arg(old_object_uuid));
        while (extension_properties_set.next()) {
            QString property_uuid = extension_properties_set.value("property_uuid").toString();
            query_strings.append(
                        QString("UPDATE schema_properties SET schema_object_uuid = '%1' "
                                "WHERE uuid = '%2';").arg(new_object_uuid).arg(property_uuid));
            query_strings.append(getWhatLogEntrySql(action_uuid, "change", "property",
                                                    "schema_object_uuid", property_uuid,
                                                    old_object_uuid, new_object_uuid));
            QString old_property_type = extension_properties_set.value("property_type").toString();
            QString new_property_type = old_property_type.remove("1").remove("#").replace("__","_").remove(QRegExp("_$")).simplified();
            query_strings.append(
                        QString("UPDATE schema_properties SET property_type = '%1' "
                                "WHERE uuid = '%2';").arg(new_property_type).arg(property_uuid));
            query_strings.append(getWhatLogEntrySql(action_uuid, "change", "property",
                                                    "property_type", property_uuid,
                                                    old_property_type, new_property_type));
        }
        //add \extension attribute to new object
        QString ext_attribute_uuid = Data->getNewUuidString();
        query_strings.append(QString("INSERT INTO schema_attributes VALUES ('%1','%2','%3','%4'); ")
                             .arg(ext_attribute_uuid)
                             .arg(new_object_uuid)
                             .arg("extension")
                             .arg("True"));
        query_strings.append(getWhatLogEntrySql(action_uuid, "add", "attribute",
                                                "extension", new_object_uuid));

        //add copy of \reference attribute to new object from old Name,
        //and move \reference attribute from Name to old Object
        QSqlQuery old_reference_qry = Data->executeQuery(
                    QString("SELECT * FROM schema_object_properties_reference "
                            "WHERE object_uuid = '%1';")
                    .arg(old_object_uuid));
        if (old_reference_qry.first()){
            QString ref_object_type = old_reference_qry.value("object_type").toString();
            QString old_reference = old_reference_qry.value("reference").toString();
            QString old_property_uuid = old_reference_qry.value("property_uuid").toString();
            if (ref_object_type.contains(QRegExp("List", Qt::CaseInsensitive)) ) {
                //"List" Object types are suspected to be pure containers, and can be skipped when _extension objects are included in the self object
                QString ref_attribute_uuid = Data->getNewUuidString();
                query_strings.append(QString("INSERT INTO schema_attributes VALUES ('%1','%2','%3','%4'); ")
                                     .arg(ref_attribute_uuid)
                                     .arg(new_object_uuid)
                                     .arg("reference")
                                     .arg(old_reference));
                query_strings.append(getWhatLogEntrySql(action_uuid, "add", "attribute",
                                                        "reference", new_object_uuid));
            }
            //TODO: investigate- this seems to make a new reference attribute on the object,
            //but leaves it on the old Label property, too
            query_strings.append(QString("REPLACE INTO schema_attributes VALUES ('%1','%2','%3','%4'); ")
                                 .arg(old_property_uuid)
                                 .arg(old_object_uuid)
                                 .arg("reference")
                                 .arg(old_reference));
            query_strings.append(getWhatLogEntrySql(action_uuid, "change", "attribute",
                                                    "reference", old_object_uuid));
        }

        //add \extension_type attribute to old object
        QString contains_attribute_uuid = Data->getNewUuidString();
        query_strings.append(QString("INSERT INTO schema_attributes VALUES ('%1','%2','%3','%4'); ")
                             .arg(contains_attribute_uuid)
                             .arg(old_object_uuid)
                             .arg("extension_type")
                             .arg(new_object_type));
        query_strings.append(getWhatLogEntrySql(action_uuid, "add", "attribute",
                                                "extension_type", contains_attribute_uuid));

        //remove extra properties, and their attributes
        QSqlQuery extension_extra_properties_set = Data->executeQuery(QString("SELECT * FROM 'schema_object_properties_position' "
                                                                              "WHERE position > %1 "
                                                                              "AND object_uuid = '%3';")
                                                                      .arg(end_position)
                                                                      .arg(old_object_uuid));
        while (extension_extra_properties_set.next()) {
            QString property_uuid = extension_extra_properties_set.value("property_uuid").toString();
            QString property_type = extension_extra_properties_set.value("property_type").toString();

            //remove attributes from property
            QSqlQuery extra_properties_attributes_set = Data->executeQuery(QString("SELECT * "
                                                                                   "FROM schema_attributes "
                                                                                   "WHERE subject_uuid = '%1'")
                                                                           .arg(property_uuid));
            while (extra_properties_attributes_set.next()) {
                QString attribute_uuid = extra_properties_attributes_set.value("uuid").toString();
                QString attribute_type = extra_properties_attributes_set.value("attribute_type").toString();
                query_strings.append(QString("DELETE FROM schema_attributes "
                                             "WHERE uuid = '%1';").arg(attribute_uuid));
                query_strings.append(getWhatLogEntrySql(action_uuid, "delete", "attribute",
                                                        attribute_type, attribute_uuid));
            }
            //remove extra property
            query_strings.append(QString("DELETE FROM schema_properties "
                                         "WHERE uuid = '%2';").arg(property_uuid));
            query_strings.append(getWhatLogEntrySql(action_uuid, "delete", "property",
                                                    property_type, property_uuid));
        }

    }

    //should be deprecated, but is needed to handle translations.
    //deprecated fields are simply ignored on the input file, as they are not ever asked for
//    query_strings.append("DELETE from schema_properties where property_type = 'Component Branch Control Type';");
//    query_strings.append(getWhatLogEntrySql(action_uuid, "delete", "property",
//                                            "Component Branch Control Type", "field no longer used"));
    executeQueryStringList(query_strings);
}

void MainWindow::alterSchema_InsertInclusionTypes()
{
    QString action_uuid = Data->getNewUuidString();
    QStringList query_strings(getWhyLogEntrySql(
                                  action_uuid, "create/insert new inclusion type for those objects that are allowed to be inserted into another",
                                  "inclusion objects allows you to drop what would have been list object members directly into the object that needs them"));

    //new interim TABLES just for this inclusion processing
    Data->executeQuery("CREATE TABLE referenceD_lookup(reference TEXT NOT NULL,object_type TEXT NOT NULL);");
    Data->executeQuery("CREATE TABLE referenceS_lookup(object_list TEXT NOT NULL,object_type TEXT NOT NULL);");

    //load up referenceD
    QSqlQuery referenceD_qry = Data->executeQuery("SELECT DISTINCT object_type, attribute_value FROM schema_object_attributes where attribute_type = 'reference' ORDER BY attribute_value;");
    QStringList reference_items;
    while (referenceD_qry.next()){
        reference_items.clear();
        reference_items.append(referenceD_qry.value("attribute_value").toString().split("|"));
        QString referenceD_type = referenceD_qry.value("object_type").toString();
        foreach (QString reference_item, reference_items) {
            Data->executeQuery(QString("INSERT INTO referenceD_lookup VALUES ('%1', '%2');")
                               .arg(reference_item)
                               .arg(referenceD_type));
        }
    }

    //load up referenceS
    QSqlQuery referenceS_qry = Data->executeQuery("SELECT DISTINCT object_list, object_type FROM schema_object_properties_object_list order by object_list;");
    QStringList object_list_items;
    while (referenceS_qry.next()){
        object_list_items.clear();
        object_list_items.append(referenceS_qry.value("object_list").toString().split("|"));
        QString referenceS_type = referenceS_qry.value("object_type").toString();
        foreach (QString object_list_item, object_list_items) {
            Data->executeQuery(QString("INSERT INTO referenceS_lookup VALUES ('%1', '%2');")
                               .arg(object_list_item)
                               .arg(referenceS_type));
        }
    }

    //for every row in referenceD_lookup...
    QSqlQuery referenceD_lookup_qry = Data->executeQuery("SELECT DISTINCT * FROM referenceD_lookup;");
    while (referenceD_lookup_qry.next()) {
        QString inclusion_object_type = referenceD_lookup_qry.value("object_type").toString();
        QString reference = referenceD_lookup_qry.value("reference").toString();
        //for every row in referenceS_lookup...
        QSqlQuery referenceS_lookup_qry = Data->executeQuery(QString("SELECT DISTINCT object_type FROM referenceS_lookup WHERE object_list = '%1';").arg(reference));
        while (referenceS_lookup_qry.next()) {
            //capture the target object_type to assign the inclusion of inclusion_object_type
            QString object_type = referenceS_lookup_qry.value("object_type").toString();
            //get uuid of referenceS object type to attach attribute
            QSqlQuery schema_object_qry = Data->executeQuery(QString("SELECT uuid FROM schema_objects WHERE object_type = '%1'").arg(object_type));
            QString insert_str;
            if (schema_object_qry.first()) {
                QString subject_uuid = schema_object_qry.value("uuid").toString();
                //attach the new inclusion attribute  //TODO:log this
                insert_str = QString("INSERT INTO schema_attributes VALUES ('%1', '%2', '%3', '%4');")
                        .arg(Data->getNewUuidString())
                        .arg(subject_uuid)
                        .arg("inclusion")
                        .arg(inclusion_object_type);
                query_strings.append(insert_str);
            } else {
                //something broke on the way here...
                qDebug() << "ERROR: on inclusion\n " << insert_str;
            }
        }
    }

    //add Vendor Object for every object
    QSqlQuery every_object_qry = Data->executeQuery("SELECT uuid FROM schema_objects;");
    QString insert_str;
    while (every_object_qry.next()) {
        QString subject_uuid = every_object_qry.value("uuid").toString();
        //attach the new inclusion attribute  //TODO:log this
        insert_str = QString("INSERT INTO schema_attributes VALUES ('%1', '%2', '%3', '%4');")
                .arg(Data->getNewUuidString())
                .arg(subject_uuid)
                .arg("inclusion")
                .arg("VendorObject");
        query_strings.append(insert_str);
    }

    executeQueryStringList(query_strings);
}

void MainWindow::alterSchema_MarkEnumerationsExtensible()
{
    QString action_uuid = Data->getNewUuidString();
    QStringList query_strings(getWhyLogEntrySql(
                                  action_uuid, "mark obviously enumerated types as extensible",
                                  "where the property "));
    QSqlQuery already_extensible_objects = Data->executeQuery("SELECT * FROM schema_select_extensible");
    QStringList already_extensible;
    while (already_extensible_objects.next()){
        already_extensible.append(already_extensible_objects.value("object_uuid").toString());
    }

    int property_count = 0;
    QSqlQuery enumerated_objects = Data->executeQuery("SELECT distinct object_uuid FROM ALTER_select_enumerated_object_properties;");
    while (enumerated_objects.next()) {
        QString object_uuid = enumerated_objects.value("object_uuid").toString();
        if (!already_extensible.contains(object_uuid)) {

            QSqlQuery enumerated_object_properties = Data->executeQuery(QString("SELECT property_uuid, object_type FROM ALTER_select_enumerated_object_properties where object_uuid = '%1';").arg(object_uuid));
            if (enumerated_object_properties.first()) {
                property_count = 1;
                QString property_uuid = enumerated_object_properties.value("property_uuid").toString();

                //add \begin-extensible to property on only the first one
                QString prop_attribute_uuid = Data->getNewUuidString();
                QString propqrystr = QString("INSERT INTO schema_attributes VALUES ('%1','%2','%3','%4'); ")
                        .arg(prop_attribute_uuid)
                        .arg(property_uuid)
                        .arg("begin_extensible")
                        .arg("True");
                query_strings.append(propqrystr);
                query_strings.append(getWhatLogEntrySql(action_uuid, "add", "attribute",
                                                        "begin_extensible", property_uuid));
            }
            while (enumerated_object_properties.next()) {
                property_count++;
            }
            //add \extensible to object
            QString obj_attribute_uuid = Data->getNewUuidString();
            QString objqrystr = QString("INSERT INTO schema_attributes VALUES ('%1','%2','%3','%4'); ")
                    .arg(obj_attribute_uuid)
                    .arg(object_uuid)
                    .arg("extensible")
                    .arg(property_count);
            query_strings.append(objqrystr);
            query_strings.append(getWhatLogEntrySql(action_uuid, "add", "attribute",
                                                    "extensible", object_uuid));
        }
    }
    executeQueryStringList(query_strings);
}

QString MainWindow::getWhatLogEntrySql(QString uuid, QString event,
                                       QString subject_type,
                                       QString subject_name,
                                       QString subject_uuid, QString old_value,
                                       QString new_value) {
    QString entry = QString("INSERT INTO what_log VALUES ( '%1', "
                            "'%2','%3','%4','%5','%6', '%7');")
            .arg(uuid)
            .arg(event)
            .arg(subject_type)
            .arg(subject_name)
            .arg(subject_uuid)
            .arg(old_value)
            .arg(new_value);
    return entry;
}

QString MainWindow::getWhyLogEntrySql(QString uuid, QString why, QString note) {
    QString entry = QString("INSERT INTO why_log VALUES ( '%1', '%2','%3');")
            .arg(uuid)
            .arg(why)
            .arg(note);
    return entry;
}

void MainWindow::putIDDintoPenTable(QString idd_filename) {
    QFile idd(idd_filename);
    if (idd.open(QFile::ReadOnly | QFile::Text)) {
        loadIDDSchema(idd);
    }
}

//////////////////////////////////////////////////////////////////////////////

QString MainWindow::cropLineType(QString line, QString type) {
    QString pattern = QString("\\\\%1 ").arg(type);
    return line.remove(QRegExp(pattern)).trimmed();
}

QStringList MainWindow::extractCleanStringListFromFile(QFile &idd) {

    QString all(idd.readAll());
    QStringList iddstrings(all.split("\n"));
    quint64 linescount = iddstrings.count();
    QStringList not_remarks;
    QStringList keepers;
    QStringList leftovers;
    QStringList multiline;
    QStringList remarks;

    foreach(QString raw_line, iddstrings) {
        if (raw_line.startsWith("!"))
            remarks.append(raw_line);
        QStringList oneline = raw_line.split("!"); // find lines with comments
        not_remarks.append(
                    oneline.value(0).trimmed()); // discard part after !, if any
    }

    int position = 0;
    foreach(QString keep, not_remarks) {
        if (keep.startsWith("\\")) { // most common case
            keepers.append(keep.trimmed());
        } else if (keep.contains(QRegExp(
                                     "[A-Z]+[0-9]+[ ]*[,|;][ ]*\\\\field "))) { // field, second
            // most common
            //            field_cnt++;
            position++;
            // tracking Field position per Object and prepending it on this line for
            // later extraction
            QString new_fieldname =
                    QString("%1~%2").arg(QString::number(position)).arg(keep.trimmed());
            keepers.append(new_fieldname);
        } else if (keep.contains(QRegExp("[A-Z]+[a-z]+([:]{1}|[A-Z]+[a-z0-9]*)*[,|;"
                                         "]"))) { // object, prepend \object to
            // normalize processing
            //            object_cnt++;
            position = 0; // reset for new Object
            QString object_string = QString("\\object %1").arg(keep.trimmed());
            keepers.append(object_string.remove(QRegExp("[,|;]$")));
            //            qDebug() << object_string;
        } else {
            if (keep.count(",") > 1)
                multiline.append(keep);
            else
                leftovers.append(keep);
        }
    }
    quint64 keepcount = keepers.count();
    quint64 remarkcount = remarks.count();
    quint64 multicount = multiline.count();
    quint64 leftcount = leftovers.count();
    qDebug() << "\nkept lines count: " << keepcount;
    qDebug() << "remark lines count: " << remarkcount;
    qDebug() << "multiline lines count: " << multicount;
    qDebug() << "leftovers lines count: " << leftcount;
    quint64 processed = keepcount + multicount + leftcount;
    qDebug() << "processed line count: " << processed;
    qDebug() << "    file lines count: " << linescount;
    qint64 diff = linescount - processed;
    if (diff == 0)
        qDebug() << "difference: " << diff;
    else {

        qDebug() << "\n#####\n#####\nERROR:   FILE LINE PROCESSING FAILED.  "
                    "\n#####\n#####\n";
        QFile multidump(QString(QDir::homePath() + "/SCRATCH/multiIDD-" +
                                QTime::currentTime().toString().remove(":") +
                                ".txt"));
        if (multidump.open(QFile::WriteOnly | QFile::Text)) {
            foreach(QString line, multiline) {
                multidump.write(line.toLocal8Bit() + "\n");
            }
            multidump.close();
        }
        QFile leftoverdump(QString(QDir::homePath() + "/SCRATCH/leftoverIDD-" +
                                   QTime::currentTime().toString().remove(":") +
                                   ".txt"));
        if (leftoverdump.open(QFile::WriteOnly | QFile::Text)) {
            foreach(QString line, leftovers) {
                leftoverdump.write(line.toLocal8Bit() + "\n");
            }
            leftoverdump.close();
        }
    }
    QFile keepdump(QString(QDir::homePath() + "/SCRATCH/IDDlines-" +
                           QTime::currentTime().toString().remove(":") + ".txt"));
    if (keepdump.open(QFile::WriteOnly | QFile::Text)) {
        foreach(QString line, keepers) {
            keepdump.write(line.toLocal8Bit() + "\n");
        }
        keepdump.close();
    }

    return keepers;
}

void MainWindow::loadIDDSchema(
        QFile &idd) { // open the file before passing it in here

    // object state items
    QString active_group = "";
    QString current_object_uuid = "";
    QString current_field_uuid = "";
    QString current_memo_uuid = "";
    QString current_memo_string = "";
    bool extensible = false;
    int field_count = 0;
    // field state items
    QString current_field_name = "";
    QString current_type_uuid = "";
    QString current_note_uuid = "";
    QString current_note_string = "";
    QString current_choice_uuid = "";
    QStringList choice_list;
    QString current_object_list_uuid = "";
    QStringList object_list_list;
    QString current_reference_uuid = "";
    QStringList reference_list;

    int min_fields =
            100; // keeps it running while get this set after 'extens' is set

    QStringList keepers = extractCleanStringListFromFile(idd);

    //    IDD->executeQuery("BEGIN TRANSACTION;");

    foreach(QString current_line, keepers) {
        if (current_line.contains("\\group ")) { // GROUP LEVEL
            active_group = cropLineType(current_line, "group");
        } else if (current_line.startsWith("\\object ")) { // OBJECT LEVEL
            min_fields = 100;
            // reset object state items
            current_object_uuid.clear();
            current_field_uuid.clear();
            current_memo_uuid.clear();
            current_memo_string.clear();
            extensible = false;
            field_count = 0;

            current_object_uuid = Data->getNewUuidString();
            QString insert_object =
                    QString("INSERT INTO schema_objects VALUES ('%1', '%2', '%3');")
                    .arg(current_object_uuid)
                    .arg(cropLineType(current_line, "object"))
                    .arg("");
            Data->executeQuery(insert_object);
            QString insert_group =
                    QString(
                        "INSERT INTO schema_attributes VALUES ('%1', '%2', '%3', '%4');")
                    .arg(Data->getNewUuidString())
                    .arg(current_object_uuid)
                    .arg("group")
                    .arg(active_group);
            Data->executeQuery(insert_group);
        } else if (current_line.startsWith("\\memo ")) {
            current_memo_string = current_memo_string.append(
                        cropLineType(current_line, "memo").append(" "));
            current_memo_uuid = (current_memo_uuid.isEmpty())
                    ? Data->getNewUuidString()
                    : current_memo_uuid;
            QString insert_string =
                    QString("REPLACE INTO schema_attributes (uuid, subject_uuid, "
                            "attribute_type, attribute_value) VALUES ('%1', '%2', '%3', "
                            "'%4');")
                    .arg(current_memo_uuid)
                    .arg(current_object_uuid)
                    .arg("memo")
                    .arg(current_memo_string.remove(",").remove("'").remove("\\").remove(";"));
            Data->executeQuery(insert_string);
        } else if (current_line.startsWith("\\unique-object")) {
            QString insert_string =
                    QString(
                        "INSERT INTO schema_attributes VALUES ('%1', '%2', '%3', '%4');")
                    .arg(Data->getNewUuidString())
                    .arg(current_object_uuid)
                    .arg("unique_object")
                    .arg("True");
            Data->executeQuery(insert_string);
        } else if (current_line.startsWith("\\required-object")) {
            QString insert_string =
                    QString(
                        "INSERT INTO schema_attributes VALUES ('%1', '%2', '%3', '%4');")
                    .arg(Data->getNewUuidString())
                    .arg(current_object_uuid)
                    .arg("required_object")
                    .arg("True");
            Data->executeQuery(insert_string);
        } else if (current_line.startsWith("\\min-fields ")) {
            min_fields = cropLineType(current_line, "min-fields").toInt();
            QString insert_string =
                    QString(
                        "INSERT INTO schema_attributes VALUES ('%1', '%2', '%3', '%4');")
                    .arg(Data->getNewUuidString())
                    .arg(current_object_uuid)
                    .arg("minimum_fields")
                    .arg(min_fields);
            Data->executeQuery(insert_string);
        } else if (current_line.startsWith("\\obsolete")) {
            QString insert_string =
                    QString(
                        "INSERT INTO schema_attributes VALUES ('%1', '%2', '%3', '%4');")
                    .arg(Data->getNewUuidString())
                    .arg(current_object_uuid)
                    .arg("obsolete")
                    .arg("True");
            Data->executeQuery(insert_string);
        } else if (current_line.startsWith("\\extensible:")) {
            extensible = true;
            QString pattern = QString("\\\\extensible:");
            current_line.remove(QRegExp(pattern)).trimmed();
            QString propstr = current_line.split(" ").at(0);
            QString insert_string =
                    QString(
                        "INSERT INTO schema_attributes VALUES ('%1', '%2', '%3', '%4');")
                    .arg(Data->getNewUuidString())
                    .arg(current_object_uuid)
                    .arg("extensible")
                    .arg(propstr);
            Data->executeQuery(insert_string);
        }
        // these only useful on the extensible-modified version IDD
        //        else if (current_line.startsWith("\\min-extent:")){
        //        } else if (current_line.startsWith("\\max-extent:")){
        //        } else if (current_line.startsWith("\\extent-type ")){
        //        }
        else if (current_line.startsWith("\\format ")) {
            QString insert_string =
                    QString(
                        "INSERT INTO schema_attributes VALUES ('%1', '%2', '%3', '%4');")
                    .arg(Data->getNewUuidString())
                    .arg(current_object_uuid)
                    .arg("format")
                    .arg(cropLineType(current_line, "format"));
            Data->executeQuery(insert_string);
        }
        // else if (good.startsWith("\\reference-class-name ")){} //never
        // encountered

        if ((!extensible) || ((extensible) && (field_count <= min_fields))) {
            if (current_line.contains("\\field ")) { // FIELD LEVEL
                field_count++;

                current_field_name.clear();
                current_type_uuid.clear();
                current_note_uuid.clear();
                current_note_string.clear();
                current_choice_uuid.clear();
                choice_list.clear();
                object_list_list.clear();
                current_reference_uuid.clear();
                reference_list.clear();

                QString line = cropLineType(current_line, "field");
                QString holder = line;
                QRegExp pos("~.*");
                QString pstn = holder.remove(
                            pos); // TODO: ensure original field position is captured here
                holder = line; // again
                QRegExp regx_front("[0-9]*~");
                QRegExp regx_back("[ ]*[,|;].*");
                QString spec = holder.remove(regx_front) // remove front
                        .remove(regx_back); // remove back
                QString field_name = line.remove(regx_front)
                        .remove(QRegExp("[A-Z]+[0-9]+[ ]*[,|;][ ]*"));
                current_field_name = field_name;
                current_field_uuid = Data->getNewUuidString();
                QString insert_schema = QString("INSERT INTO schema_properties VALUES "
                                                "('%1', '%2', '%3', '%4');")
                        .arg(current_field_uuid)
                        .arg(current_object_uuid)
                        .arg(current_field_name)
                        .arg("");
                Data->executeQuery(insert_schema);

                QString insert_string = QString("INSERT INTO schema_attributes VALUES "
                                                "('%1', '%2', '%3', '%4');")
                        .arg(Data->getNewUuidString())
                        .arg(current_field_uuid)
                        .arg("specifier")
                        .arg(spec);
                Data->executeQuery(insert_string);
                QString position_string = QString("INSERT INTO schema_attributes "
                                                  "VALUES ('%1', '%2', '%3', '%4');")
                        .arg(Data->getNewUuidString())
                        .arg(current_field_uuid)
                        .arg("position")
                        .arg(pstn);
                Data->executeQuery(position_string);

            } else if (current_line.startsWith("\\note ")) {
                current_note_string = current_note_string.append(
                            cropLineType(current_line, "note").append(" "));
                current_note_uuid = (current_note_uuid.isEmpty())
                        ? Data->getNewUuidString()
                        : current_note_uuid;
                QString insert_string =
                        QString("REPLACE INTO schema_attributes (uuid, subject_uuid, "
                                "attribute_type, attribute_value) VALUES ('%1', '%2', "
                                "'%3', '%4');")
                        .arg(current_note_uuid)
                        .arg(current_field_uuid)
                        .arg("note")
                        .arg(current_note_string.remove(",").remove("'").remove("\\").remove(";"));
                Data->executeQuery(insert_string);
            } else if (current_line.startsWith("\\required-field")) {
                QString position_string = QString("INSERT INTO schema_attributes "
                                                  "VALUES ('%1', '%2', '%3', '%4');")
                        .arg(Data->getNewUuidString())
                        .arg(current_field_uuid)
                        .arg("required_field")
                        .arg("True");
                Data->executeQuery(position_string);
            } else if (current_line.startsWith("\\begin-extensible")) {
                QString position_string = QString("INSERT INTO schema_attributes "
                                                  "VALUES ('%1', '%2', '%3', '%4');")
                        .arg(Data->getNewUuidString())
                        .arg(current_field_uuid)
                        .arg("begin_extensible")
                        .arg("True");
                Data->executeQuery(position_string);
            } else if (current_line.startsWith("\\type ")) {
                current_type_uuid = (current_type_uuid.isEmpty())
                        ? Data->getNewUuidString()
                        : current_type_uuid;
                QString insert_schema = QString("REPLACE INTO schema_properties VALUES "
                                                "('%1', '%2', '%3', '%4');")
                        .arg(current_field_uuid)
                        .arg(current_object_uuid)
                        .arg(current_field_name)
                        .arg(cropLineType(current_line, "type"));
                Data->executeQuery(insert_schema);
                // TODO: correct this redundancy
                QString position_string = QString("REPLACE INTO schema_attributes "
                                                  "VALUES ('%1', '%2', '%3', '%4');")
                        .arg(Data->getNewUuidString())
                        .arg(current_field_uuid)
                        .arg("data_type")
                        .arg(cropLineType(current_line, "type"));
                Data->executeQuery(position_string);
            } else if (current_line.startsWith("\\units ")) {
                QString position_string = QString("INSERT INTO schema_attributes "
                                                  "VALUES ('%1', '%2', '%3', '%4');")
                        .arg(Data->getNewUuidString())
                        .arg(current_field_uuid)
                        .arg("units")
                        .arg(cropLineType(current_line, "units"));
                Data->executeQuery(position_string);
            } else if (current_line.startsWith("\\ip-units ")) {
                QString position_string =
                        QString("INSERT INTO schema_attributes VALUES ('%1', '%2', '%3', "
                                "'%4');")
                        .arg(Data->getNewUuidString())
                        .arg(current_field_uuid)
                        .arg("ip-units")
                        .arg(cropLineType(current_line, "ip-units"));
                Data->executeQuery(position_string);
            } else if (current_line.startsWith("\\unitsBasedOnField ")) {
                QString position_string =
                        QString("INSERT INTO schema_attributes VALUES ('%1', '%2', '%3', "
                                "'%4');")
                        .arg(Data->getNewUuidString())
                        .arg(current_field_uuid)
                        .arg("units_based_on_field")
                        .arg(cropLineType(current_line, "unitsBasedOnField"));
                Data->executeQuery(position_string);
            } else if (current_line.startsWith("\\minimum ")) {
                QString position_string =
                        QString("INSERT INTO schema_attributes VALUES ('%1', '%2', '%3', "
                                "'%4');")
                        .arg(Data->getNewUuidString())
                        .arg(current_field_uuid)
                        .arg("minimum")
                        .arg(cropLineType(current_line, "minimum"));
                Data->executeQuery(position_string);
            } else if (current_line.startsWith("\\minimum> ")) {
                QString position_string =
                        QString("INSERT INTO schema_attributes VALUES ('%1', '%2', '%3', "
                                "'%4');")
                        .arg(Data->getNewUuidString())
                        .arg(current_field_uuid)
                        .arg("exclude_minimum")
                        .arg(cropLineType(current_line, "minimum>"));
                Data->executeQuery(position_string);
            } else if (current_line.startsWith("\\maximum ")) {
                QString position_string =
                        QString("INSERT INTO schema_attributes VALUES ('%1', '%2', '%3', "
                                "'%4');")
                        .arg(Data->getNewUuidString())
                        .arg(current_field_uuid)
                        .arg("maximum")
                        .arg(cropLineType(current_line, "maximum"));
                Data->executeQuery(position_string);
            } else if (current_line.startsWith("\\maximum< ")) {
                QString position_string =
                        QString("INSERT INTO schema_attributes VALUES ('%1', '%2', '%3', "
                                "'%4');")
                        .arg(Data->getNewUuidString())
                        .arg(current_field_uuid)
                        .arg("exclude_maximum")
                        .arg(cropLineType(current_line, "maximum<"));
                Data->executeQuery(position_string);
            } else if (current_line.startsWith("\\default ")) {
                QString position_string =
                        QString("INSERT INTO schema_attributes VALUES ('%1', '%2', '%3', "
                                "'%4');")
                        .arg(Data->getNewUuidString())
                        .arg(current_field_uuid)
                        .arg("value_default")
                        .arg(cropLineType(current_line, "default"));
                Data->executeQuery(position_string);
            } else if (current_line.startsWith("\\deprecated")) {
                QString position_string = QString("INSERT INTO schema_attributes "
                                                  "VALUES ('%1', '%2', '%3', '%4');")
                        .arg(Data->getNewUuidString())
                        .arg(current_field_uuid)
                        .arg("deprecated")
                        .arg("True");
                Data->executeQuery(position_string);
            } else if (current_line.startsWith("\\autosizable")) {
                QString position_string = QString("INSERT INTO schema_attributes "
                                                  "VALUES ('%1', '%2', '%3', '%4');")
                        .arg(Data->getNewUuidString())
                        .arg(current_field_uuid)
                        .arg("autosizable")
                        .arg("True");
                Data->executeQuery(position_string);
            } else if (current_line.startsWith("\\autocalculatable")) {
                QString position_string = QString("INSERT INTO schema_attributes "
                                                  "VALUES ('%1', '%2', '%3', '%4');")
                        .arg(Data->getNewUuidString())
                        .arg(current_field_uuid)
                        .arg("autocalculatable")
                        .arg("True");
                Data->executeQuery(position_string);
            } else if (current_line.startsWith("\\retaincase")) {
                QString position_string = QString("INSERT INTO schema_attributes "
                                                  "VALUES ('%1', '%2', '%3', '%4');")
                        .arg(Data->getNewUuidString())
                        .arg(current_field_uuid)
                        .arg("retaincase")
                        .arg("True");
                Data->executeQuery(position_string);
            } else if (current_line.startsWith("\\key ")) { // append each new 'key'
                current_choice_uuid = (current_choice_uuid.isEmpty())
                        ? Data->getNewUuidString()
                        : current_choice_uuid;
                QString this_key = cropLineType(current_line, "key");
                choice_list.append(this_key);
                choice_list.sort();
                QString choices_str = QString("REPLACE INTO schema_attributes VALUES "
                                              "('%1', '%2', '%3', '%4');")
                        .arg(current_choice_uuid)
                        .arg(current_field_uuid)
                        .arg("choices")
                        .arg(choice_list.join("|"));
                Data->executeQuery(choices_str);
            } else if (current_line.startsWith("\\object-list ")) {// append each new 'object-list'
                current_object_list_uuid = (current_object_list_uuid.isEmpty())
                        ? Data->getNewUuidString()
                        : current_object_list_uuid;
                QString this_obj_list = cropLineType(current_line, "object-list");
                object_list_list.append(this_obj_list);
                object_list_list.sort();
                QString position_string =
                        QString("REPLACE INTO schema_attributes VALUES ('%1', '%2', '%3', "
                                "'%4');")
                        .arg(Data->getNewUuidString())
                        .arg(current_field_uuid)
                        .arg("object_list")
                        .arg(object_list_list.join("|"));
                Data->executeQuery(position_string);
            } else if (current_line.startsWith("\\external-list ")) {
                QString position_string =
                        QString("INSERT INTO schema_attributes VALUES ('%1', '%2', '%3', "
                                "'%4');")
                        .arg(Data->getNewUuidString())
                        .arg(current_field_uuid)
                        .arg("external_list")
                        .arg(cropLineType(current_line, "external-list"));
                Data->executeQuery(position_string);
            } else if (current_line.startsWith(
                           "\\reference ")) { // append each new 'reference'
                current_reference_uuid = (current_reference_uuid.isEmpty())
                        ? Data->getNewUuidString()
                        : current_reference_uuid;
                QString this_ref = cropLineType(current_line, "reference");
                reference_list.append(this_ref);
                reference_list.sort();
                QString reference_str = QString("REPLACE INTO schema_attributes VALUES "
                                                "('%1', '%2', '%3', '%4');")
                        .arg(current_reference_uuid)
                        .arg(current_field_uuid)
                        .arg("reference")
                        .arg(reference_list.join("|"));
                Data->executeQuery(reference_str);
            }
        }
    }
    //    IDD->executeQuery("COMMIT;");

    qDebug("done processing IDD");
}

void MainWindow::putIDFintoTranslator(QString idf_filename)
{
    QFile idf(idf_filename);
    if (idf.open(QFile::ReadOnly | QFile::Text)) {
        IDF->loadIDFModel(idf);
    }
}


void MainWindow::generateIDDxSchema() {
    updateSchema();
    Data->saveToDisk(QString(QDir::homePath() + "/SCRATCH/pentable-IDDx" +
                             QTime::currentTime().toString().remove(":") +
                             ".sqlt"));
}

void MainWindow::on_sql_generate_clicked() {
    generateIDDxSchema();
}

void MainWindow::on_sql_idd_edit_textEdited(const QString &idd) {
    QString newiid = idd;
    delete Data;
    Data = new PenTable();

    ui->sql_selector->clear();

    resetIddModelGeneration(newiid.remove("file://"));
}

void MainWindow::on_sql_selector_2_currentIndexChanged(const QString &newview)
{
    QString qrystr = QString("SELECT * FROM '%1'").arg(newview);
    submitDataViewQuery(qrystr);
}

void MainWindow::on_sql_filter_2_returnPressed()
{
    submitDataViewQuery(ui->sql_filter_2->text());
}

void MainWindow::on_SQL_view_2_doubleClicked(const QModelIndex &index)
{
    QString item_name = Data_proxy_model->data(index).toString();
    QString column_name =
            Data_proxy_model->headerData(index.column(), Qt::Horizontal)
            .toString();
    submitDataViewQuery(QString("SELECT * FROM model_object_properties "
                                "WHERE %1 like '%2'; ")
                        .arg(column_name)
                        .arg(item_name));
    ui->SQL_view_2->resizeColumnsToContents();

    //    ui->xml_xml_find->setText(item_name);
}

void MainWindow::on_SQL_view_doubleClicked(const QModelIndex &index) {
    QString item_name = Schema_proxy_model->data(index).toString();
    QString column_name =
            Schema_proxy_model->headerData(index.column(), Qt::Horizontal)
            .toString();
    submitSchemaViewQuery(QString("SELECT * FROM schema_object_properties_attributes "
                                  "WHERE %1 like '%2'; ")
                          .arg(column_name)
                          .arg(item_name));
    ui->SQL_view->resizeColumnsToContents();

    //    ui->xml_xsd_find->setText(item_name);
}

void MainWindow::on_sql_selector_currentIndexChanged(const QString &newview) {
    QString qrystr = QString("SELECT * FROM '%1'").arg(newview);
    submitSchemaViewQuery(qrystr);
}

void MainWindow::on_sql_filter_returnPressed() {
    submitSchemaViewQuery(ui->sql_filter->text());
}

void MainWindow::submitSchemaViewQuery(QString qry) {
    qDebug() << qry;
    ui->sql_filter->setText(qry);
    qDebug() << Data->updateSchemaViewModelQuery(qry);
    ui->SQL_view->resizeColumnsToContents();
}

void MainWindow::submitDataViewQuery(QString qry)
{
    qDebug() << qry;
    ui->sql_filter_2->setText(qry);
    qDebug() <<  Data->updateDataViewModelQuery(qry);
    ui->SQL_view_2->resizeColumnsToContents();
}

void MainWindow::datatypeChanges() {
    // this is a separate function, as these must be called in this order
    alterSchema_SetDataTypeObjectList();
    alterSchema_SetMissingUnitDataTypes();
    alterSchema_DataTypeCase();
    alterSchema_AlphaDataTypeSpecifierA();
    alterSchema_SetDataTypeRealByValues();
    alterSchema_SetDataTypeIntegerByValues();
    alterSchema_SetDataTypeIntegerByValues();
    alterSchema_SetDataTypeNameInference();
    alterSchema_SetDataTypeExternalList();
    alterSchema_EnsureAttributeDataTypeEntries();
}

void MainWindow::nameChanges()
{
    alterSchema_NameToLabel();
    // alterSchema_IllegalCharacterFix();
    // alterSchema_ObjectListEnumerations();
}

void MainWindow::updateSchema() {
    QElapsedTimer timer;
    timer.start();

    alterSchema_RemoveObsolete();
  //  alterSchema_RemoveDeprecated(); //removing "deprecated" at this point fouls the idf importer. will need to deal with this elsewhere
    nameChanges();
    datatypeChanges();
    alterSchema_SetOptionalRequired();
    alterSchema_ExtensibleExtraction();
    //    alterSchema_InsertInclusionTypes();

    qDebug() << "update schema execution time: " << timer.elapsed();
}

void MainWindow::on_sql_idf_edit_textEdited(const QString &idf)
{
    QString filename = idf;
    putIDFintoTranslator(filename.remove("file://"));
}

void MainWindow::on_sql_idfx_generate_clicked()
{
    IDF->translate();
}

void MainWindow::on_schema_validate_clicked()
{
    validateXml();
}

void MainWindow::on_xml_pretty_clicked()
{
    QString allfile = ui->xml_xml->toPlainText();
    allfile.replace("\" ", "\"\n");
    ui->xml_xml->setPlainText(allfile);
}

void MainWindow::on_xml_xml_find_textChanged(const QString &arg1)
{


    QString searchString = ui->xml_xml_find->text();
    QTextDocument *document = ui->xml_xml->document();

    on_xml_highlight_clicked();
    delete xml_highlight;

    bool found = false;
    int count = 0;

    if (isFirstTimeXML == false)
        document->undo();


    QTextCursor highlightCursor(document);
    QTextCursor cursor(document);

    cursor.beginEditBlock();

    QTextCharFormat plainFormat(highlightCursor.charFormat());
    QTextCharFormat colorFormat = plainFormat;
    colorFormat.setBackground(Qt::cyan);

    while (!highlightCursor.isNull() && !highlightCursor.atEnd()) {
        highlightCursor = document->find(searchString, highlightCursor);

        if (!highlightCursor.isNull()) {
            found = true;
            count++;
            highlightCursor.movePosition(QTextCursor::Right,
                                         QTextCursor::KeepAnchor);
            highlightCursor.mergeCharFormat(colorFormat);
        }

        cursor.endEditBlock();

        isFirstTimeXML = false;

    }
    reportMessage("found: " + count);

}

void MainWindow::on_xml_xsd_find_textChanged(const QString &arg1)
{

    QString searchString = ui->xml_xsd_find->text();
    QTextDocument *document = ui->xml_xsd->document();

    on_xml_highlight_clicked();
    delete xsd_highlight;

    bool found = false;
    int count = 0;

    if (isFirstTimeXSD == false)
        document->undo();


    QTextCursor highlightCursor(document);
    QTextCursor cursor(document);

    cursor.beginEditBlock();

    QTextCharFormat plainFormat(highlightCursor.charFormat());
    QTextCharFormat colorFormat = plainFormat;
    colorFormat.setBackground(Qt::cyan);

    while (!highlightCursor.isNull() && !highlightCursor.atEnd()) {
        highlightCursor = document->find(searchString, highlightCursor);

        if (!highlightCursor.isNull()) {
            found = true;
            count++;
            highlightCursor.movePosition(QTextCursor::Right,
                                         QTextCursor::KeepAnchor);
            highlightCursor.mergeCharFormat(colorFormat);
        }

        cursor.endEditBlock();

        isFirstTimeXSD = false;

    }
    reportMessage("found: " + count);

}

void MainWindow::on_xml_xml_find_textEdited(const QString &arg1)
{
    on_xml_xml_find_textChanged(arg1);

}

void MainWindow::on_xml_highlight_clicked()
{
    //    ui->xml_xml_find->clear();
    xml_highlight = new XmlSyntaxHighlighter(ui->xml_xml->document());

}

void MainWindow::on_xml_xsd_find_textEdited(const QString &arg1)
{
    on_xml_xsd_find_textChanged(arg1);
}

void MainWindow::on_xsd_highlight_clicked()
{
    xsd_highlight = new XmlSyntaxHighlighter(ui->xml_xsd->document());
}

void MainWindow::exportJSON(bool nice)
{
    QFile jsonfile(QDir::homePath() + "/SCRATCH/idd.json");
    if (jsonfile.open(QFile::WriteOnly | QFile::Text))
    {
        QJsonDocument json_document;
        QJsonObject json_root_object;

        //object
        QString oqry = QString("select object_type, uuid from schema_objects order by ROWID;");
        //            qDebug() << oqry;
        QSqlQuery ordered_objects = Data->executeQuery(oqry);

        while (ordered_objects.next()){

            QJsonObject json_object;
            QString curr_object = ordered_objects.value("object_type").toString();
            QString curr_object_uuid = ordered_objects.value("uuid").toString();
            qDebug() << curr_object;



            //object attributes
            QString aqry = QString("select attribute_type, attribute_value from schema_attributes where subject_uuid = '%1' order by attribute_type;").arg(curr_object_uuid);
            QSqlQuery ordered_attributes = Data->executeQuery(aqry);
            while (ordered_attributes.next()) {
                QString attr_type = ordered_attributes.value("attribute_type").toString();
                if ((attr_type != "minimum_fields") && (attr_type != "extensible") && (attr_type != "format")){
                    QString attr_value = ordered_attributes.value("attribute_value").toString();
                    //if attr_type a list type, split on | and put in array
                    if ((attr_value.contains("|")) && (!attr_type.contains("memo"))) {
                        QJsonArray json_choices;
                        QStringList choices = attr_value.split("|");
                        foreach (QString choice, choices) {
                            json_choices.append(choice.remove("\"").remove("\\").trimmed());
                        }
                        json_object.insert(attr_type, json_choices);
                    } else {
                        json_object.insert(attr_type, attr_value.remove("\"").remove("\\").trimmed());
                    }
                }
            }





            //properties
            QString qry = QString("select property_type, property_uuid from schema_object_properties where object_type = '%1' order by ROWID;").arg(curr_object);
            //                qDebug() << qry;
            QSqlQuery ordered_properties = Data->executeQuery(qry);

            while (ordered_properties.next()){
                //property attributes
                QJsonObject json_property;
                QString prop_type = ordered_properties.value("property_type").toString();
                if (prop_type != "order") {
                    QString prop_uuid = ordered_properties.value("property_uuid").toString();

                    QString datatype_qry = QString("select data_type from schema_properties where uuid = '%1';").arg(prop_uuid);
                    QSqlQuery prop_datatype = Data->executeQuery(datatype_qry);
                    if (prop_datatype.first()) {
                        json_property.insert("data_type", prop_datatype.value("data_type").toString());
                    } else {
                        qDebug() << "FAIL: missing data_type! " << prop_uuid << " : " << prop_type;
                    }

                    QString qry = QString("select attribute_type, attribute_value from schema_attributes where subject_uuid = '%1' order by attribute_type;").arg(prop_uuid);
                    QSqlQuery ordered_attributes = Data->executeQuery(qry);
                    while (ordered_attributes.next()) {
                        QString attr_type = ordered_attributes.value("attribute_type").toString();
                        if ((attr_type != "begin_extensible") && (attr_type != "retaincase")  && (attr_type != "data_type")){
                            if (attr_type.contains("position"))
                                attr_type = "idd_position";
                            if (attr_type.contains("specifier"))
                                attr_type = "idd_specifier";
                            QString attr_value = ordered_attributes.value("attribute_value").toString();
                            //if attr_type a list type, split on | and put in array
                            if (attr_value.contains("|") && (!attr_type.contains("note"))) {
                                QJsonArray json_choices;
                                QStringList choices = attr_value.split("|");
                                foreach (QString choice, choices) {
                                    json_choices.append(choice.remove("\"").remove("\\").trimmed());
                                }
                                json_property.insert(attr_type, json_choices);
                            } else if (attr_type.contains("imum") || attr_type.contains("default") || attr_type.contains("_position")) {

                                if (attr_type.contains("_position")){ //ugly, just get it done!
                                    json_property.insert(attr_type, attr_value.toInt());
                                } else {
                                    //get data type
                                    QString dqry = QString("select attribute_value from schema_attributes where subject_uuid = '%1' and attribute_type = 'data_type';").arg(prop_uuid);
                                    QString data_type;
                                    QSqlQuery dtype = Data->executeQuery(dqry);
                                    if (dtype.first()) {
                                        data_type = dtype.value("attribute_value").toString();
                                    }
                                    if (data_type == "integer") {
                                        json_property.insert(attr_type, attr_value.toInt());
                                    } else if (data_type == "real") {
                                        json_property.insert(attr_type, attr_value);
                                    }
                                }
                            } else if (attr_type.contains("required_field") || attr_type.contains("auto")) {
                                //get data type
                                QString dqry = QString("select attribute_value from schema_attributes where subject_uuid = '%1' and attribute_type = 'data_type';").arg(prop_uuid);
                                QString data_type;
                                QSqlQuery dtype = Data->executeQuery(dqry);
                                if (dtype.first())
                                    data_type = dtype.value("attribute_value").toString();
                                json_property.insert(attr_type, QJsonValue(true));
                            } else {
                                json_property.insert(attr_type, attr_value.remove("\"").remove("\\").trimmed());
                            }
                        }
                    }
                    json_object.insert(prop_type, json_property);
                }
            }



            json_root_object.insert(curr_object.toLocal8Bit(),json_object);
        }
        json_document.setObject(json_root_object);
        if (nice) {
            jsonfile.write(json_document.toJson(QJsonDocument::Indented));
        } else {
            jsonfile.write(json_document.toJson(QJsonDocument::Compact));
        }
        jsonfile.close();
    }
}

void MainWindow::on_pushButton_clicked() //generate json idd
{
    exportJSON();
}

void MainWindow::on_pushButton_2_clicked() //generate code file of schema
{
    on_pushButton_clicked();
    QFile jsonfile(QDir::homePath() + "/SCRATCH/idd.json");
    if (jsonfile.open(QFile::ReadOnly | QFile::Text))
    {
        QString allfile = jsonfile.readAll();
        QFile codefile(QDir::homePath() + "/SCRATCH/idd-full.h");
        if (codefile.open(QFile::WriteOnly | QFile::Text | QFile::Truncate))
        {
            QString newfile("#include <string>\nstd::string getSchema(){\nreturn std::string(\"");
            newfile.append(allfile.replace("\"", "\\\""));//escape quotes in .h file
            newfile.append("\");}\n");
            codefile.write(newfile.toLocal8Bit());
            codefile.close();
        } else {
            qDebug()  << "ERROR: json code file failed to open - " << codefile.errorString();
        }
    } else {
        qDebug()  << "ERROR: json idd file failed to open - " << jsonfile.errorString();
    }

}

void MainWindow::on_pushButton_3_clicked()
{
    exportJSON(true);
}

void MainWindow::on_pushButton_4_clicked()
{ //export the ordered IDD data
    QFile jsonfile(QDir::homePath() + "/SCRATCH/IDDx-ordered.json");
    if (jsonfile.open(QFile::WriteOnly | QFile::Text))
    {
        QJsonDocument json_document;
        QJsonObject json_root_object;

        QString oqry = QString("select object_type from schema_objects order by ROWID;");
        //            qDebug() << oqry;
        QSqlQuery ordered_objects = Data->executeQuery(oqry);

        while (ordered_objects.next()){

            QJsonObject json_object;
            QString curr_object = ordered_objects.value(0).toString();
            qDebug() << curr_object;
            QString qry = QString("select property_type, property_uuid from schema_object_properties where object_type = '%1' order by ROWID;").arg(curr_object);
            //                qDebug() << qry;
            QSqlQuery ordered_properties = Data->executeQuery(qry);
            QJsonArray json_property;
            while (ordered_properties.next()){

                QString prop_type = ordered_properties.value("property_type").toString();
                json_property.append(prop_type);
            }
            json_root_object.insert(curr_object.toLocal8Bit(),json_property);
        }
        json_document.setObject(json_root_object);
        jsonfile.write(json_document.toJson(QJsonDocument::Compact));
        jsonfile.close();
    }

}

void MainWindow::on_pushButton_5_clicked()
{
    on_pushButton_4_clicked();
    QFile jsonfile(QDir::homePath() + "/SCRATCH/IDDx-ordered.json");
    if (jsonfile.open(QFile::ReadOnly | QFile::Text))
    {
        QString allfile = jsonfile.readAll();
        QFile codefile(QDir::homePath() + "/SCRATCH/idd-minimal-ordered.h");
        if (codefile.open(QFile::WriteOnly | QFile::Text | QFile::Truncate))
        {
            QString newfile("#include <string>\nstd::string getIDD(){\nreturn std::string(\"");
            newfile.append(allfile.replace("\"", "\\\""));//escape quotes in .h file
            newfile.append("\");}\n");
            codefile.write(newfile.toLocal8Bit());
            codefile.close();
        } else {
            qDebug()  << "ERROR: json code file failed to open - " << codefile.errorString();
        }
    } else {
        qDebug()  << "ERROR: json idd file failed to open - " << jsonfile.errorString();
    }
}
