CREATE TABLE schema_objects(
uuid TEXT PRIMARY KEY,
object_type TEXT NOT NULL,
parent_uuid TEXT);

CREATE TABLE schema_properties(
uuid TEXT PRIMARY KEY,
schema_object_uuid NOT NULL,
property_type TEXT NOT NULL,
data_type TEXT NOT NULL,
FOREIGN KEY (schema_object_uuid)
REFERENCES schema_objects(uuid));

CREATE TABLE schema_attributes(
uuid TEXT PRIMARY KEY,
subject_uuid TEXT NOT NULL,
attribute_type TEXT NOT NULL,
attribute_value TEXT NOT NULL);

CREATE TABLE model_objects(
uuid TEXT PRIMARY KEY,
object_type TEXT NOT NULL,
parent_uuid TEXT);

CREATE TABLE model_properties(
uuid TEXT PRIMARY KEY,
property_type TEXT NOT NULL,
property_value TEXT,
model_object_uuid TEXT NOT NULL,
FOREIGN KEY (model_object_uuid)
REFERENCES model_objects(uuid));

-- views into root tables
-- schema views
CREATE VIEW
schema_object_properties
AS SELECT
o.object_type,
p.property_type,
p.data_type,
o.uuid AS object_uuid,
p.uuid AS property_uuid
FROM
schema_objects o,
schema_properties p
WHERE
p.schema_object_uuid = o.uuid;

CREATE VIEW
schema_object_properties_attributes
AS SELECT
sop.object_type,
sop.property_type,
sop.data_type,
a.attribute_type,
a.attribute_value,
sop.object_uuid,
sop.property_uuid,
a.uuid AS attribute_uuid
FROM
schema_object_properties sop,
schema_attributes a
WHERE
a.subject_uuid = sop.property_uuid;

CREATE VIEW
schema_object_attributes
AS SELECT
o.object_type,
a.attribute_type,
a.attribute_value,
o.uuid AS object_uuid,
a.uuid AS attribute_uuid
FROM
schema_objects o,
schema_attributes a
WHERE
a.subject_uuid = o.uuid;

CREATE VIEW schema_object_attribute_types
AS SELECT DISTINCT attribute_type
FROM schema_object_attributes
ORDER BY attribute_type;

CREATE VIEW schema_property_attribute_types
AS SELECT DISTINCT attribute_type
FROM schema_object_properties_attributes
ORDER BY attribute_type;

CREATE VIEW schema_roots
AS SELECT *
FROM schema_objects
WHERE parent_uuid = ''
order by object_type;

CREATE VIEW schema_object_data
AS select
object_type,
property_type,
attribute_type,
attribute_value
from
schema_object_properties_attributes
where
attribute_type != 'position'
order by
object_type,
property_type;

CREATE VIEW
schema_object_properties_position
AS SELECT
object_type,
property_type,
CAST(attribute_value AS number) AS position,
object_uuid,
property_uuid
FROM
schema_object_properties_attributes
WHERE attribute_type = 'position';

CREATE VIEW schema_object_properties_position_datatype
AS SELECT
p.object_type,
p.property_type,
p.position,
d.data_type,
p.object_uuid,
p.property_uuid
FROM
schema_object_properties_position p,
schema_object_properties d
WHERE p.property_uuid = d.property_uuid
ORDER BY p.object_type, p.position;

CREATE VIEW schema_object_property_types
AS SELECT object_type, property_type, data_type
FROM schema_object_properties_position_datatype;

CREATE VIEW schema_select_extensible
as select object_type, attribute_type, attribute_value, object_uuid, attribute_uuid
from schema_object_attributes
where attribute_type = 'extensible';

CREATE VIEW schema_select_extensible_types
as select object_type
from schema_object_attributes
where attribute_type = 'extensible';

CREATE VIEW schema_select_extensible_type_objects
as select object_type, object_uuid
from schema_object_attributes
where attribute_type = 'extensible';

CREATE VIEW schema_select_extensible_extension_types
as select distinct
object_type,
attribute_value AS extension_type,
object_uuid
from
schema_object_attributes
where
attribute_type = 'extension_type'
order by object_type;

CREATE VIEW schema_select_extension_types
as select object_type
from schema_object_attributes
where attribute_type = 'extension';

CREATE VIEW schema_select_extension_type_objects
as select object_type, object_uuid
from schema_object_attributes
where attribute_type = 'extension';

CREATE VIEW schema_select_object_extension_type_objects
as SELECT
o.object_type,
t.object_type as 'extension_type',
o.object_uuid,
t.object_uuid as 'extension_object_uuid'
FROM
schema_select_extensible_extension_types o,
schema_select_extension_type_objects t
where
o.extension_type = t.object_type;

CREATE VIEW
schema_select_object_list_values
AS SELECT attribute_value, object_uuid
FROM schema_object_properties_attributes
WHERE attribute_type = 'object_list';

CREATE VIEW
schema_object_reference_type
AS SELECT object_type,
attribute_value AS 'reference',
object_uuid
FROM schema_object_attributes
where attribute_type = 'reference';


CREATE VIEW view_schema_IDDx
AS SELECT *
FROM schema_objects
WHERE parent_uuid = '';

-- model/schema views
CREATE VIEW model_object_properties
AS SELECT
o.object_type,
p.property_type,
p.property_value,
o.uuid AS object_uuid,
p.uuid AS property_uuid,
p.model_object_uuid,
o.parent_uuid
FROM
model_objects o,
model_properties p
WHERE
p.model_object_uuid = o.uuid;

CREATE VIEW model_object_properties_attributes
AS SELECT DISTINCT
o.object_type,
p.property_type,
p.property_value,
s.attribute_type,
s.attribute_value,
p.object_uuid,
p.property_uuid
FROM
model_objects o,
model_object_properties p,
schema_object_properties_attributes s
WHERE
p.model_object_uuid = o.uuid
AND o.object_type = s.object_type
AND p.property_type = s.property_type;

CREATE VIEW model_object_root
AS SELECT
o.object_type,
o.uuid
FROM
model_objects o
WHERE
o.parent_uuid = '';

CREATE VIEW model_object_parents
AS select
o.uuid,
o.object_type
from
model_objects o,
model_object_root r
where
o.parent_uuid = r.uuid;

CREATE VIEW model_select_object_ordered_property_values
as SELECT distinct
object_type,
attribute_value,
property_type,
property_value,
object_uuid,
property_uuid
FROM 'model_object_properties_attributes'
where attribute_type = 'position'
order by object_type, cast(attribute_value  as INTEGER);

CREATE VIEW model_select_extensible
as select distinct m.object_type, m.uuid as 'model_object_uuid', s.extension_type, s.extension_object_uuid as 'schema_extention_object_uuid'
from model_objects m, schema_select_object_extension_type_objects s
where m.object_type in schema_select_extensible_types
and m.object_type = s.object_type;

CREATE VIEW model_select_object_lists
AS SELECT distinct object_type, property_type, property_value, object_uuid, property_uuid
FROM model_object_properties_attributes
where attribute_type = 'data_type'
AND attribute_value = 'object_list';

-- change log tables/views
CREATE TABLE what_log(
uuid TEXT NOT NULL,
event TEXT NOT NULL,
subject_type TEXT NOT NULL,
subject_name TEXT NOT NULL,
subject_uuid TEXT NOT NULL,
old_value TEXT,
new_value TEXT,
FOREIGN KEY (uuid)
REFERENCES why_log(uuid));

CREATE TABLE why_log(
uuid TEXT NOT NULL,
why TEXT NOT NULL,
note TEXT);

CREATE VIEW 'log What Changed'
AS SELECT
event AS 'Event',
subject_type AS 'Type',
subject_name AS 'Name',
old_value AS 'Old',
new_value AS 'New'
FROM
what_log;

CREATE VIEW 'log Why Changed'
AS SELECT
why AS 'Event',
note AS 'Note'
FROM
why_log;

CREATE VIEW 'log Why & Wherefore'
AS SELECT
why AS 'Why',
event AS 'How',
subject_type AS 'What',
subject_name AS 'Where',
old_value AS 'old',
new_value AS 'new'
FROM
why_log y
NATURAL JOIN
what_log t;

-- property attribute views
CREATE VIEW schema_object_properties_minimum AS SELECT object_type, property_type, CAST(attribute_value AS number) AS 'minimum', object_uuid, property_uuid FROM schema_object_properties_attributes WHERE attribute_type = 'minimum';
CREATE VIEW schema_object_properties_maximum AS SELECT object_type, property_type, CAST(attribute_value AS number) AS 'maximum', object_uuid, property_uuid FROM schema_object_properties_attributes WHERE attribute_type = 'maximum';
CREATE VIEW schema_object_properties_exclude_minimum AS SELECT object_type, property_type, CAST(attribute_value AS number) AS 'exclude_minimum', object_uuid, property_uuid FROM schema_object_properties_attributes WHERE attribute_type = 'exclude_minimum';
CREATE VIEW schema_object_properties_exclude_maximum AS SELECT object_type, property_type, CAST(attribute_value AS number) AS 'exclude_maximum', object_uuid, property_uuid FROM schema_object_properties_attributes WHERE attribute_type = 'exclude_maximum';

CREATE VIEW schema_object_properties_autocalculatable AS SELECT object_type, property_type, attribute_value AS autocalculatable, object_uuid, property_uuid FROM schema_object_properties_attributes WHERE attribute_type = 'autocalculatable';
CREATE VIEW schema_object_properties_autosizable AS SELECT object_type, property_type, attribute_value AS autosizable, object_uuid, property_uuid FROM schema_object_properties_attributes WHERE attribute_type = 'autosizable';
CREATE VIEW schema_object_properties_begin_extensible AS SELECT object_type, property_type, attribute_value AS begin_extensible, object_uuid, property_uuid FROM schema_object_properties_attributes WHERE attribute_type = 'begin_extensible';
CREATE VIEW schema_object_properties_choices AS SELECT object_type, property_type, attribute_value AS choices, object_uuid, property_uuid FROM schema_object_properties_attributes WHERE attribute_type = 'choices';
CREATE VIEW schema_object_properties_data_type AS SELECT object_type, property_type, attribute_value AS data_type, object_uuid, property_uuid FROM schema_object_properties_attributes WHERE attribute_type = 'data_type';
CREATE VIEW schema_object_properties_ip_units AS SELECT object_type, property_type, attribute_value AS 'ip_units', object_uuid, property_uuid FROM schema_object_properties_attributes WHERE attribute_type = 'ip-units';
CREATE VIEW schema_object_properties_note AS SELECT object_type, property_type, attribute_value AS note, object_uuid, property_uuid FROM schema_object_properties_attributes WHERE attribute_type = 'note';
CREATE VIEW schema_object_properties_object_list AS SELECT object_type, property_type, attribute_value AS object_list, object_uuid, property_uuid FROM schema_object_properties_attributes WHERE attribute_type = 'object_list';
CREATE VIEW schema_object_properties_reference AS SELECT object_type, property_type, attribute_value AS reference, object_uuid, property_uuid FROM schema_object_properties_attributes WHERE attribute_type = 'reference';
CREATE VIEW schema_object_properties_required_field AS SELECT object_type, property_type, attribute_value AS required_field, object_uuid, property_uuid FROM schema_object_properties_attributes WHERE attribute_type = 'required_field';
CREATE VIEW schema_object_properties_retaincase AS SELECT object_type, property_type, attribute_value AS retaincase, object_uuid, property_uuid FROM schema_object_properties_attributes WHERE attribute_type = 'retaincase';
CREATE VIEW schema_object_properties_specifier AS SELECT object_type, property_type, attribute_value AS specifier, object_uuid, property_uuid FROM schema_object_properties_attributes WHERE attribute_type = 'specifier';
CREATE VIEW schema_object_properties_units AS SELECT object_type, property_type, attribute_value AS units, object_uuid, property_uuid FROM schema_object_properties_attributes WHERE attribute_type = 'units';
CREATE VIEW schema_object_properties_value_default AS SELECT object_type, property_type, attribute_value AS value_default, object_uuid, property_uuid FROM schema_object_properties_attributes WHERE attribute_type = 'value_default';

CREATE VIEW schema_object_attributes_inclusion AS SELECT object_type, attribute_value AS inclusion, object_uuid FROM schema_object_attributes WHERE attribute_type = 'inclusion';

-- ALTER views used in schema change code

CREATE VIEW 'ALTER_begin_extensible'
as select object_type, property_type, data_type, attribute_type, attribute_value, object_uuid, property_uuid, attribute_uuid
from schema_object_properties_attributes
where attribute_type = 'begin_extensible';

CREATE VIEW 'ALTER_extensible_with_begin_extensible'
as select object_type, attribute_type, attribute_value, object_uuid, attribute_uuid from 'schema_select_extensible' where object_type in (select object_type from ALTER_begin_extensible);

CREATE VIEW 'ALTER_select_extensible'
as select object_type, attribute_type, attribute_value, object_uuid, attribute_uuid
from schema_object_attributes
where attribute_type = 'extensible';

CREATE VIEW 'ALTER_select_obsolete'
AS SELECT object_type, attribute_type, object_uuid, attribute_uuid
FROM 'schema_object_attributes'
WHERE attribute_type = 'obsolete';

CREATE VIEW 'ALTER_select_deprecated'
AS SELECT
object_type,
property_type,
attribute_type,
attribute_value,
property_uuid,
attribute_uuid
FROM 'schema_object_properties_attributes'
WHERE attribute_type = 'deprecated'
AND attribute_value = 'True';

CREATE VIEW 'ALTER_select_name'
AS SELECT
property_type,
attribute_type,
attribute_value,
property_uuid
FROM
'schema_object_properties_attributes'
WHERE
property_type = 'Name';

CREATE VIEW 'ALTER_deprecate_label_attribute_retaincase'
AS select property_type, attribute_type, attribute_uuid
from 'schema_object_properties_attributes'
where property_type = 'Label'
and attribute_type = 'retaincase';

CREATE VIEW 'ALTER_deprecate_label_attribute_reference'
AS select property_type, attribute_type, attribute_uuid
from 'schema_object_properties_attributes'
where property_type = 'Label'
and attribute_type = 'reference';

CREATE VIEW 'ALTER_deprecate_label_attribute_required_field'
AS select property_type, attribute_type, attribute_uuid
from 'schema_object_properties_attributes'
where property_type = 'Label'
and attribute_type = 'required_field';

CREATE VIEW ALTER_label_without_datatype
AS SELECT * FROM 'schema_object_properties_attributes' where data_type = ''
and property_type = 'Label';

CREATE VIEW ALTER_blank_datatype_specifier_A
AS SELECT * FROM 'schema_object_properties_attributes' where data_type = ''
and (attribute_type = 'specifier'
and attribute_value like 'A%');

CREATE VIEW 'ALTER_alpha_datatype_objectlist'
AS SELECT *
FROM 'schema_object_properties_attributes' where data_type = 'alpha'
and attribute_type = 'object_list';

CREATE VIEW 'ALTER_blank_datatype_objectlist'
AS SELECT *
FROM 'schema_object_properties_attributes'
where data_type = ''
and attribute_type = 'object_list';

CREATE VIEW 'ALTER_normalize_datatype_objectlist'
AS SELECT *
FROM 'schema_object_properties_attributes'
where (data_type = 'Object-list'
or data_type = 'object-list')
and attribute_type = 'object_list';


CREATE VIEW 'ALTER_more_normalize_datatype_objectlist'
AS SELECT *
FROM 'schema_object_properties_attributes'
where attribute_value = 'object-list';


CREATE VIEW 'ALTER_normalize_datatype_alpha'
AS SELECT *
FROM 'schema_object_properties_attributes'
where data_type = 'Alpha';

CREATE VIEW 'ALTER_normalize_datatype_choice'
AS SELECT *
FROM 'schema_object_properties_attributes'
where data_type = 'Choice';

CREATE VIEW 'ALTER_set_unit_datatype_real'
AS SELECT *
FROM 'schema_object_properties_attributes'
WHERE data_type = ''
and attribute_type = 'units'
and attribute_value != 'hr'
and attribute_value != 'hh:mm'
and attribute_value != 'days';

CREATE VIEW 'ALTER_set_unit_datatype_integer'
AS SELECT *
FROM 'schema_object_properties_attributes'
WHERE data_type = ''
and attribute_type = 'units'
and (attribute_value = 'hr'
or attribute_value = 'days');

CREATE VIEW 'ALTER_set_unit_datatype_alpha'
AS SELECT *
FROM 'schema_object_properties_attributes'
WHERE data_type = ''
and attribute_type = 'units'
and attribute_value = 'hh:mm';

CREATE VIEW 'ALTER_set_decimal_minimum_datatype_real'
AS SELECT *
FROM 'schema_object_properties_attributes'
where data_type = ''
and (attribute_type = 'minimum'
and attribute_value like '%.%');

CREATE VIEW 'ALTER_set_decimal_maximum_datatype_real'
AS SELECT *
FROM 'schema_object_properties_attributes'
where data_type = ''
and (attribute_type = 'maximum'
and attribute_value like '%.%');

CREATE VIEW 'ALTER_set_decimal_ex_minimum_datatype_real'
AS SELECT *
FROM 'schema_object_properties_attributes'
where data_type = ''
and (attribute_type = 'exclude_minimum'
and attribute_value like '%.%');

CREATE VIEW 'ALTER_set_decimal_ex_maximum_datatype_real'
AS SELECT *
FROM 'schema_object_properties_attributes'
where data_type = ''
and (attribute_type = 'exclude_maximum'
and attribute_value like '%.%');

CREATE VIEW 'ALTER_set_decimal_default_datatype_real'
AS SELECT *
FROM 'schema_object_properties_attributes'
where data_type = ''
and (attribute_type = 'default'
and attribute_value like '%.%');

CREATE VIEW 'ALTER_set_not_decimal_minimum_datatype_integer'
AS SELECT *
FROM 'schema_object_properties_attributes'
where data_type = ''
and (attribute_type = 'minimum'
and attribute_value not like '%.%');

CREATE VIEW 'ALTER_set_not_decimal_maximum_datatype_integer'
AS SELECT *
FROM 'schema_object_properties_attributes'
where data_type = ''
and (attribute_type = 'maximum'
and attribute_value not like '%.%');

CREATE VIEW 'ALTER_set_not_decimal_ex_minimum_datatype_integer'
AS SELECT *
FROM 'schema_object_properties_attributes'
where data_type = ''
and (attribute_type = 'exclude_minimum'
and attribute_value not like '%.%');

CREATE VIEW 'ALTER_set_not_decimal_ex_maximum_datatype_integer'
AS SELECT *
FROM 'schema_object_properties_attributes'
where data_type = ''
and (attribute_type = 'exclude_maximum'
and attribute_value not like '%.%');

CREATE VIEW 'ALTER_set_not_decimal_default_datatype_integer'
AS SELECT *
FROM 'schema_object_properties_attributes'
where data_type = ''
and (attribute_type = 'default'
and attribute_value not like '%.%');


CREATE VIEW 'ALTER_set_integer_datatype_priority'
AS SELECT * FROM 'schema_object_properties_attributes'
where data_type = ''
and property_type = 'Priority';

CREATE VIEW 'ALTER_set_integer_datatype_year'
AS SELECT * FROM 'schema_object_properties_attributes'
where data_type = ''
and property_type like '%Year';

CREATE VIEW 'ALTER_set_integer_datatype_debugdata'
AS SELECT * FROM 'schema_object_properties_attributes'
where data_type = ''
and object_type = 'Output:DebuggingData';

CREATE VIEW 'ALTER_select_properties_external_list'
AS SELECT *
FROM schema_properties
WHERE data_type like 'external-list';

CREATE VIEW 'ALTER_select_attributes_external_list'
AS SELECT *
FROM schema_attributes
WHERE attribute_value like 'external-list';

CREATE VIEW 'ALTER_object_list_enumerations'
AS SELECT * FROM 'schema_object_properties_attributes' where attribute_type = 'choices' and attribute_value like '%:%' and property_type != 'Report_Type';

CREATE VIEW ALTER_select_enumerated_object_properties
as select distinct object_type, property_type, data_type, object_uuid, property_uuid
from schema_object_properties
where (property_type like '%_1%'
or property_type like 'Outside_Layer')
and property_type not like '%_10%'
and property_type not like '%_11%'
and property_type not like '%_12%'
and property_type not like '%_13%'
and property_type not like '%_14%'
and property_type not like '%_15%'
and property_type not like '%_16%'
and property_type not like '%_17%'
and property_type not like '%_18%'
and property_type not like '%_19%'
and property_type not like '%_21%'
and property_type not like '%_31%'
and property_type not like '%_41%'
and property_type not like '%_51%'
and property_type not like '%_61%'
and property_type not like '%_71%'
and property_type not like '%_81%'
and property_type not like '%_91%'
and property_type not like '%_100%'
and property_type not like '%_201%'
and property_type not like '%_Y-%'
and property_type not like '%_Z-%'
and property_type not like 'CO2%'
and property_type not like 'N2O%'
and property_type not like 'PM10%'
and property_type not like 'PM2.5%'
and property_type not like 'NH3%'
and property_type not like '%Coefficient%'
and property_type not like '%Factor%'
and property_type not like '%Function%'
and property_type not like '%Hour%'
and property_type not like '%Week%'
and property_type not like '%Month%'
and property_type not like '%Day%'
and property_type not like '%0 or 1%'
and property_type not like '%Greater than%'
and object_type not like '%Coil:Cooling:DX:TwoStageWithHumidityControlMode%'
and object_type not like '%Coil:Heating:WaterToAirHeatPump:ParameterEstimation%'
and object_type not like '%Coil:UserDefined%'
and object_type not like '%AirTerminal:SingleDuct:ConstantVolume:CooledBeam%'
and object_type not like '%Fan:ComponentModel%'
and object_type not like '%Generator:FuelCell:ExhaustGasToWaterHeatExchanger%'
and object_type not like '%GroundHeatTransfer:Slab:BldgProps%'
and object_type not like '%PhotovoltaicPerformance:Sandia%'
and object_type not like '%Refrigeration:AirChiller%'
and object_type not like '%WaterHeater:Stratified%'
and object_type not like '%PlantComponent:UserDefined%'
and object_type not like '%AirLoopHVAC:UnitaryHeatPump:AirToAir:MultiSpeed%'
and object_type not like '%ZoneControl:Thermostat:StagedDualSetpoint%'

order by object_type;



CREATE VIEW ALTER_beginextensible_position
AS SELECT
p.object_type,
p.property_type,
cast(p.position AS integer),
d.data_type,
p.object_uuid,
p.property_uuid
FROM
schema_object_properties_position p,
schema_object_properties d,
schema_object_properties_attributes a
WHERE p.property_uuid = d.property_uuid
AND a.property_uuid = d.property_uuid
AND a.attribute_type = 'begin_extensible'
ORDER BY p.object_type, p.position;


-- below here are schema exploration/alteration views, not meant to be carried to the translator itself
-- potential to be extensible
CREATE VIEW 'list_object-list_type_value'
AS SELECT object_type, property_type
FROM schema_object_properties_attributes
WHERE data_type = 'object-list'
and attribute_type = 'object_list';

CREATE VIEW 'list_extensible_without_begin_extensible'
as select object_type, attribute_type, attribute_value, object_uuid, attribute_uuid from 'schema_select_extensible' where object_type not in (select object_type from ALTER_begin_extensible);

CREATE VIEW 'list_min_fields'
as select object_type, attribute_type, attribute_value, object_uuid, attribute_uuid
from schema_object_attributes
where attribute_type = 'minimum_fields';


create view list_model_constructions as
SELECT distinct object_type, property_type, property_value, attribute_value, object_uuid FROM model_select_object_ordered_property_values WHERE object_type like 'Construction' and property_value != 'UNUSED_OPTIONAL_FIELD' order by object_uuid, cast (attribute_value as integer);
