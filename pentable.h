/**********************************************************************
        Copyright 2014 Shannon Mackey

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

        http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
**********************************************************************/

#ifndef PENTABLE_H
#define PENTABLE_H

#include <map>

#include <QFile>
#include <QSqlDatabase>
#include <QSqlQueryModel>

class PenTable
{
public:
    PenTable();
    ~PenTable();


    void execSqlScript(QString sql_filename, QSqlDatabase &db);
    void putIDDintoSQL(QString idd_file);
    QSqlQuery executeQuery(QString query_string);
    bool saveToDisk(QString filename);

    void generateSchemaDataViews();
    void generateModelDataViews();

    QString getNewUuidString();

    bool insertModelObject(QString unique_id, QString type, QString parent_id);
    bool setModelProperty(QString unique_id, QString type, QString value, QString object_id);


    //QSql stuff
    QSqlQueryModel *getSchemaViewModel();
    QSqlQueryModel *getDataViewModel();
    QStringList getViews(QString keyterm);
    QString updateSchemaViewModelQuery(QString query);
    QString updateDataViewModelQuery(QString query);

    //QObject stuff
    QObject *getSchemaObjectModel(QString schema_name);
    QObject *getDataObjectModel(QString model_name);

    void getDataObjectProperties(QObject *child_object, QString object_uuid);
private:
    QSqlDatabase DB;
    QString LastError;

    QSqlQueryModel *SchemaViewModel;
    QSqlQueryModel *DataViewModel;
    void initializeActiveDB(QString sql_filename);

    //IDD load
    void loadSchemaObjectModelFromIDD(QFile &idd);
    QStringList extractCleanStringListFromFile(QFile &idd);
    QString cropLineType(QString line, QString type);


    //QObject stuff
    void fillSchemaObjectProperties(QObject *head, QString name, QString uuid);
    void fillSchemaPropertyProperties(QObject *head, QString uuid);

};

class PTObject
{
public:
    PTObject();
    ~PTObject();
};

#endif // PENTABLE_H
