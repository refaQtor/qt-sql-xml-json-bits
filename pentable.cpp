/**********************************************************************
        Copyright 2014 Shannon Mackey

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

        http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
**********************************************************************/

#include "pentable.h"

//#include <utility>
//#include <regex>

#include <QStringList>
#include <QSqlQuery>
#include <QSqlError>
#include <QUuid>
#include <QDebug>
#include <QDir>
#include <QTime>


const QString SQLFILE("://resources/pentable-root-tables.sql");

PenTable::PenTable():
    LastError(""),
    SchemaViewModel(new QSqlQueryModel()),
    DataViewModel(new QSqlQueryModel())
{
    initializeActiveDB(SQLFILE);
}

PenTable::~PenTable()
{
    DB.close();
    delete SchemaViewModel;
    delete DataViewModel;
}


QString PenTable::getNewUuidString(){
    return QUuid::createUuid().toString().remove("{").remove("}");
}
//////////////////////////////////
/// \brief PenTable::insertModelObject
/// \param unique_id
/// \param type
/// \param parent_id
/// \return
///////////////////////////////////
bool PenTable::insertModelObject(QString unique_id, QString type, QString parent_id)
{
    QSqlQuery insert_object(DB);
    insert_object.prepare(QString("INSERT INTO model_objects VALUES ('%1', '%2', '%3');")
                          .arg(unique_id).arg(type).arg(parent_id));
    if (!insert_object.exec()) {
        LastError = insert_object.lastError().text();
        qDebug() << "Model object insertion failed: " << LastError;
        return false;
    } else
        return true;
}

////////////////////////////////////
/// \brief PenTable::setModelProperty
/// \param unique_id
/// \param type
/// \param value
/// \param object_id
/// \return
/////////////////////////////////////
bool PenTable::setModelProperty(QString unique_id, QString type, QString value, QString object_id)
{
    QSqlQuery insert_property(DB);
    insert_property.prepare(QString("REPLACE INTO model_properties VALUES ('%1', '%2', '%3', '%4');")
                            .arg(unique_id).arg(type).arg(value).arg(object_id));
    if (!insert_property.exec()) {
        LastError = insert_property.lastError().text();
        qDebug() << "Model property insertion failed: "  << LastError;
        return false;
    } else
        return true;
}

QSqlQueryModel *PenTable::getSchemaViewModel()
{
    return SchemaViewModel;
}

QSqlQueryModel *PenTable::getDataViewModel()
{
    return DataViewModel;
}

QStringList PenTable::getViews(QString keyterm)
{
    QStringList retlist;
    foreach (QString view, DB.tables(QSql::Views)) {
        if (view.startsWith(keyterm))
            retlist.append(view);
    }
    return retlist;
}

QString PenTable::updateSchemaViewModelQuery(QString query)
{
    SchemaViewModel->setQuery(query,DB);
    return SchemaViewModel->lastError().text();
}

QString PenTable::updateDataViewModelQuery(QString query)
{
    DataViewModel->setQuery(query,DB);
    return DataViewModel->lastError().text();
}

void PenTable::getDataObjectProperties(QObject *child_object, QString object_uuid)
{
    QString get_properties = QString("SELECT property_type, property_value FROM model_properties WHERE model_object_uuid = '%1';")
            .arg(object_uuid);
    QSqlQuery object_properties(get_properties,DB);
    while (object_properties.next()){
        QString property_type = object_properties.value("property_type").toString();
        QString property_value = object_properties.value("property_value").toString();
        qDebug() << property_type << " : " << property_value;
        child_object->setProperty(property_type.toLocal8Bit(), property_value);
    }
}

QObject *PenTable::getDataObjectModel(QString model_name)
{
// this could, perhaps, be more elegant - but, now, it only satisfies our
    //two-level-deep object((property)/(object(property)) pattern
    QObject *return_object = new QObject();
    return_object->setObjectName(model_name);
    QString get_objects = QString("SELECT * FROM model_object_parents;");
    QSqlQuery model_objects(get_objects,DB);
    while (model_objects.next()){
        QObject *parent_object = new QObject(return_object);
        QString object_uuid = model_objects.value("uuid").toString();
        parent_object->setProperty("uuid", object_uuid);
        parent_object->setObjectName(model_objects.value("object_type").toString()/*.replace(":","_")*/);
        getDataObjectProperties(parent_object, object_uuid);
//        qDebug() << parent_object;
        QString get_children = QString("SELECT * FROM model_objects WHERE parent_uuid = '%1';")
                .arg(object_uuid);
        QSqlQuery object_children(get_children,DB);
        while (object_children.next()){
            QObject *child_object = new QObject(parent_object);
            QString object_uuid = object_children.value("uuid").toString();
            child_object->setProperty("uuid", object_uuid);
            child_object->setObjectName(object_children.value("object_type").toString()/*.replace(":","_")*/);
//            qDebug() << " ~~~~ " << child_object;
            getDataObjectProperties(child_object, object_uuid);
        }
    }
    return return_object;
}



QObject *PenTable::getSchemaObjectModel(QString schema_name)
{
    // make qobject called schema_name
    // set uuid property on this, if needed
    // get type/uuid for all objects that point at uuid
    // make object for each, as children of returned object
    // call fillObject on each
    QObject *return_object = new QObject();
    return_object->setObjectName(schema_name);
    QString get_objects = QString("select object_type, uuid from schema_objects;");
    QSqlQuery schema_objects(get_objects,DB);
    while (schema_objects.next()){
        fillSchemaObjectProperties(new QObject(return_object),
                                   schema_objects.value("object_type").toString(),
                                   schema_objects.value("uuid").toString());
    }

    return return_object;
}

void PenTable::fillSchemaObjectProperties(QObject *head, QString name, QString uuid)
{
    // get all attributes pointing at uuid
    // for each, set property/value on head
    head->setObjectName(name);
    fillSchemaPropertyProperties(head,uuid);

    // get all properties pointing at me
    // call fillOject on them, which make them children of this
    QString get_objects = QString("select property_type, property_uuid from schema_object_properties where object_type = '%1' order by ROWID;")
            .arg(name);
    QSqlQuery schema_objects(get_objects,DB);
    while (schema_objects.next()){
        QObject * prop_obj = new QObject(head);
        prop_obj->setObjectName(schema_objects.value("property_type").toString());
        fillSchemaPropertyProperties(prop_obj, schema_objects.value("property_uuid").toString());
    }
}

void PenTable::fillSchemaPropertyProperties(QObject *head, QString uuid)
{
    // get all attributes pointing at uuid
    // for each, set property/value on head
    head->setProperty("uuid",uuid);
    QString get_attributes = QString("SELECT attribute_type, attribute_value FROM schema_attributes WHERE subject_uuid = '%1';")
            .arg(uuid);
    QSqlQuery object_attributes(get_attributes,DB);
    while (object_attributes.next()){
        QString attribute_type = object_attributes.value("attribute_type").toString();
        QString attribute_value = object_attributes.value("attribute_value").toString();
        head->setProperty(attribute_type.toLocal8Bit(), attribute_value);
    }
}

QSqlQuery PenTable::executeQuery(QString query_string)
{
    QSqlQuery return_qry(DB);
    return_qry.prepare(query_string);
    if (!return_qry.exec()) {
        qDebug() << "\n\n" << query_string << "----" << return_qry.lastError();
    }
    return return_qry;
}

bool PenTable::saveToDisk(QString filename)
{
    QSqlDatabase DiskDB = QSqlDatabase::addDatabase("QSQLITE","DISK");
    DiskDB.setDatabaseName(filename);
    if (DiskDB.open()) {
        execSqlScript(SQLFILE, DiskDB);
        QSqlQuery attach(DB);
        QString attachstr = QString("ATTACH DATABASE '%1' AS %2")
                .arg(filename)
                .arg(DiskDB.connectionName());
        if (!attach.exec(attachstr)) {
            qDebug() << "ERROR: openDiskDB database ATTACH failed- "
                     << attach.lastError();
        }
        QSqlQuery insertquery(DB);
        QStringList tablesviews = DB.tables(QSql::Tables);
        foreach (QString tableorview, tablesviews) {
            QString insertstr =
                    QString("INSERT INTO %2.%1 SELECT * FROM '%1'")
                    .arg(tableorview)
                    .arg(DiskDB.connectionName());
            if (!insertquery.exec(insertstr)){
                qDebug() << "ERROR: openDiskDB INSERT failed - "
                         << insertquery.lastError() << insertstr;
            }
        }
        QSqlQuery detach(DB);
        QString detachstr = QString("DETACH %1")
                .arg(DiskDB.connectionName());
        detach.exec(detachstr);
        DiskDB.close();
    }  else {
        QString error_string =
                QString("ERROR: database :memory: at MEM failed to open - ")
                .arg(DiskDB.lastError().text());
        qDebug() << error_string;
    }
}

void PenTable::generateSchemaDataViews()
{

}

void PenTable::generateModelDataViews()
{

}

void PenTable::execSqlScript(QString sql_filename, QSqlDatabase & db)
{
    QFile script(sql_filename);
    if (script.open(QFile::ReadOnly | QFile::Text)) {
        QString scriptstring(script.readAll());
        QStringList queries = scriptstring.split(QChar(';'));
        queries.removeAll("^\n$");
        QSqlQuery query(db);
        foreach(QString queryString, queries){
            if (!queryString.isEmpty()) {
                if (!query.exec(queryString.trimmed())){
                    QString error_string =
                            QString("ERROR: query failed - %1")
                            .arg(query.lastError().text());
                    qDebug() << error_string << queryString;
                }
            }
        }
        //            qDebug() << MemDB.tables(QSql::AllTables);
        script.close();
    } else {
        QString error_string =
                QString("ERROR: %1 failed to open - %2")
                .arg(script.fileName())
                .arg(script.errorString());
        qDebug() << error_string;
    }
}

void PenTable::initializeActiveDB(QString sql_filename)
{
    DB = QSqlDatabase::addDatabase("QSQLITE","ACTIVE");
    DB.setDatabaseName(":memory:");
    if (DB.open()) {
        execSqlScript(SQLFILE, DB);
    }  else {
        QString error_string =
                QString("ERROR: database :memory: failed to open - ")
                .arg(DB.lastError().text());
        qDebug() << error_string;
    }
}



///////////////////////////////////////
/// \brief PTObject::PTObject
//////////////////////////////////////
PTObject::PTObject()
{

}

PTObject::~PTObject()
{

}
