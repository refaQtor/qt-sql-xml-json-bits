#-------------------------------------------------
#
# Project created by QtCreator 2014-06-26T15:13:55
#
#-------------------------------------------------

QT       += core gui sql xml xmlpatterns

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = XMLStreamWriter
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    xmlsyntaxhighlighter.cpp \
    pentable.cpp \
    idftranslator.cpp

HEADERS  += mainwindow.h \
    xmlsyntaxhighlighter.h \
    pentable.h \
    idftranslator.h

FORMS    += mainwindow.ui

OTHER_FILES += \
    resources/Energy+v8-1-0-009-utf8.iddx \
    resources/Energy+v8-1-0-009-utf8-short.iddx \
    resources/Energy+v8-1-0-009-utf8-PRE-EXTENSIBLE.iddx.txt \
    resources/IDDxSchema.sql \
    resources/pentable-root-tables.sql \
    resources/IDD_training_set.idd \
    resources/ASHRAE90.1_RestaurantFastFood_STD2010_Chicago_V810.idf \
    resources/extensible.ods \
    resources/idd_training_set_object_list.txt

RESOURCES += \
    resources.qrc
