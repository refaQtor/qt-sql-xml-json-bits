/**********************************************************************
        Copyright 2014 Shannon Mackey

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

        http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
**********************************************************************/

#include "mainwindow.h"
#include <QApplication>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    MainWindow w;
    w.show();

    return a.exec();
}



//TODO: properly handle enumeration text case problems like "YES" for "Yes"
//TODO: properly handle integer/float problems like "1.0000" for xs:integer
//TODO: properly handle autocalc/size values in both validation and conversion, can these fields be dropped when auto..=default? or something like that





//TODO: xsd- distill one way link types in xsd objects
//TODO: xsd- drop "Template" objects
//TODO: trans- preprocess all example files and cebecs with ExpandObjects
//TODO: trans- preprocess all example files and cebecs with Parametric
//TODO: trans- ground connection preprocess

//TODO: fix Table:MultiVariableLookup, extensible mismatch with begin-extensible
//TODO: fix 'ip-units' not set to 'ip_units'
//TODO: fix logging of some "change" "property" entries that duplicate name, but miss uuid
